clear all; close all; clc; 

% Problem Set 8 Calculations 
% Constants
global Re mu_e j2
Re      = 6378.137;      % km
Rm      = 3396.19;       % km
Rv      = 6051.80;       % km
Rp      = 1195.00;       % km
Rl      = 1737.40;       % km
Rj      = 71492.0;       % km
aE      = 149649952;     % km
aP      = 5868125070;    % km 
am      = 227953016;     % km
al      = 384400;        % km
aj      = 779067093;     % km
mu_e    = 398600.4418;   % km^3/s^2 
mu_m    = 42828.37190;   % km^3/s^2 
mu_v    = 324858.592079; % km^3/s^2
mu_p    = 873.7674473980320; % km^3/s^2
mu_s    = 132712200000.00; 
mu_l    = 4902.801076;
mu_j    = 126686535;
j2      = 0; %0.0010826269;
deg2rad = pi/180; 
rad2deg = 180/pi;

%% Problem 1 (from PS8 Fall 2017)
% Given 
rPark   = Re + 180; % 300; % 
rC      = 7*Rp; % 11*Rp; %

% part (a) 
vPark   = sqrt(mu_e/(rPark));
aT      = 1/2*(aE+aP);
vE      = sqrt(mu_s/aE);
vPlus   = sqrt(2*mu_s/aE - mu_s/aT);
vinfp   = vPlus - vE;
vphyper1 = sqrt(2*(vinfp^2/2 + mu_e/(rPark))) ;
dv1     = vphyper1 - vPark;

vP      = sqrt(mu_s/aP);
vC      = sqrt(mu_p/rC);
dv2     = vphyper1 - vC;
vMinus  = sqrt(2*mu_s/aP - mu_s/aT);
vinfm   = vP - vMinus; 
vphyper = sqrt(2*(vinfm^2/2 + mu_p/rC));
dv3     = vphyper - vC;

dvt     = dv1 + dv3;

% part (b)
ra      = 30*Rp; % 11*Rp; %
rp      = 2*Rp;
a       = 1/2*(ra + rp);
vp      = sqrt(2*mu_p/rp - mu_p/a);
va      = sqrt(2*mu_p/ra - mu_p/a);

% periapsis capture
dv2     = vphyper1 - vp;
vMinus  = sqrt(2*mu_s/aP - mu_s/aT);
vinfm   = vP - vMinus; 
vphyper = sqrt(2*(vinfm^2/2 + mu_p/rp));
dv3     = vphyper - vp;

dvt     = dv1 + dv3;

% apoapsis capture
dv2     = vphyper1 - va;
vMinus  = sqrt(2*mu_s/aP - mu_s/aT);
vinfm   = vP - vMinus; 
vphyper = sqrt(2*(vinfm^2/2 + mu_p/ra));
dv3     = vphyper - va;

dvt     = dv1 + dv3;

%% Problem 2 (from PS9 Fall 2017)
% Given 
rp  = 32*Rj; %570000 + Rj; %  

% part (a) 
jd1 = jday(2006, 1, 19, 0, 0, 0);
jd2 = jday(2007, 2, 28, 0, 0, 0);
TOFd = jd2-jd1; 
TOFy = TOFd/365.25;

% part (b) and (e)
aT      = 1/2*(aE+aP);     %1.5117e9; %       
eT      = 1-aE/aT; % (aT - aE)/aT; %0.901;    %    
pT      = aT*(1-eT^2);
% thetaT  = acosd((pT/aj-1)/eT);
thetaT  = acosd(pT/aj/eT - 1/eT);
vMinus  = sqrt(2*(mu_s/aj-mu_s/2/aT));
fpaMinus= acosd(sqrt(mu_s*pT)/aj/vMinus);
ET      = acos((1-aj/aT)/eT); 
TOFT    = (ET-eT*sin(ET))*sqrt(aT^3/mu_s)/86400;
phi     = (thetaT*deg2rad - sqrt(mu_s/aj^3)*TOFT*86400)*rad2deg;

% part (d)
dv_esc  = vPark*sqrt(2)-vPark;
dv_diff = dv1 - dv_esc;

% part (e) 
vMinVec = [vMinus*sind(fpaMinus);vMinus*cosd(fpaMinus)];

% part (f)
vJ      = sqrt(mu_s/aj); 
vinfm   = sqrt(vMinus^2 + vJ^2 - 2*vMinus*vJ*cosd(fpaMinus));
ahyper  = mu_j/vinfm^2; 
ehyper  = 1 + rp/abs(ahyper);
delta   = 2*asind(1/ehyper); 
rPlus   = aj;
nu      = acosd((vJ^2 + vinfm^2 - vMinus^2)/2/vJ/vinfm);
vPlus   = sqrt(vinfm^2 + vJ^2 - 2*vinfm*vJ*cosd(delta + nu));
fpaPlus = acosd((vPlus^2 + vJ^2 - vinfm^2)/2/vJ/vPlus);
aPlus   = -mu_s/(vPlus^2/2-mu_s/aj)/2;
ePlus   = sqrt((rPlus*vPlus^2/mu_s - 1)^2*((cosd(fpaPlus))^2 + (sin(fpaPlus))^2));
pPlus   = aPlus*(1-ePlus^2);
taPlus  = atand((rPlus*vPlus^2/mu_s)*cosd(fpaPlus)*sind(fpaPlus)/((rPlus*vPlus^2/mu_s)*cosd(fpaPlus)^2-1));
EnerP   = mu_s/abs(aPlus)/2;
domega  = thetaT - taPlus;
rpPlus  = abs(aPlus*(ePlus-1)); % not asked in 2018 PS8
dv_eq   = sqrt(vinfm^2 + vinfm^2 - 2*vinfm*vinfm*cosd(delta));
crack   = 180 - nu - fpaMinus;
weed    = acosd((vinfm^2 + dv_eq^2 - vinfm^2)/2/dv_eq/vinfm); 
alpha   = 180 - crack - weed;
Hplus   = 2*atanh(tand(taPlus/2)*((ePlus-1)/(ePlus+1))^0.5);

veq_vec = dv_eq*[cosd(alpha);sind(alpha)];

% part (g) 
TA    = linspace(86,131,100);
TA1   = linspace(0,360,100);

for i = 1:size(TA,2)
    r1(i) = pPlus/(1+ePlus*cos(TA(1,i)*pi/180));
    rT(i) = pT/(1+eT*cos(TA(1,i)*pi/180));
    rP(i) = rPark/(1+0*cos(TA(1,i)*pi/180));
end

for i = 1:size(TA1,2)
    rP(i) = rPark/(1+0*cos(TA1(1,i)*pi/180));
end

for i = 1:size(TA,2)
    e_r1(i) = r1(i)*cos(TA(i)*pi/180);
    theta_r1(i) = r1(i)*sin(TA(i)*pi/180);
    e_rT(i) = rT(i)*cos(TA(i)*pi/180);
    theta_rT(i) = rT(i)*sin(TA(i)*pi/180);
    eP(i) = rP(i)*cos(TA(i)*pi/180);
    thetaP(i) = rP(i)*sin(TA(i)*pi/180);
end
for i = 1:size(TA1,2)
    eP(i) = rP(i)*cos(TA(i)*pi/180);
    thetaP(i) = rP(i)*sin(TA(i)*pi/180);
end

figure(2)
% plot(e_rT,theta_rT,'r-','LineWidth',2); grid on; hold on;
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
% plot(eP,thetaP,'g-','LineWidth',2); 
plot(0,0,'ko',0,0,'k.','MarkerSize',12);
axis equal
xlabel('ehat, km'); ylabel('phat, km');



%% Problem 3 
% Given
rc_e     = Re + 200; %Re + 180; % 185; % 
rc_l     = Rl + 140; %Rl + 120; % 150; % 
TAnew    = 172.5; % 173.1; 

%% part (a) (from PS8 Fall 2017)
rpT      = rc_e;
raT      = al;
aT       = 1/2*(rpT+raT);
eT       = 1-rpT/aT; %(aT - rpT)/aT;
EnerT    = -mu_e/2/aT; 
aTPeriod = 2*pi*sqrt(aT^3/mu_e);
lPeriod  = 2*pi*sqrt(al^3/mu_e);
TOF      = 1/2*aTPeriod;
vc_E     = sqrt(mu_e/rc_e);               % circular speed of parking orbit
vpT      = sqrt(2*mu_e/rpT - mu_e/aT);    % speed of transfer orbit at periapsis
vaT      = sqrt(2*mu_e/raT - mu_e/aT);
dv1      = vpT - vc_E;                    % delta-v required to leave parking orbit
taMinus  = 180;  
fpaMinus = 0;
rMinus   = al;
vMinus   = sqrt(2*mu_e/raT - mu_e/aT);    % speed of transfer orbit at apoapsis
vl       = sqrt(mu_e/rMinus);             % circular speed of lunar parking orbit
dvMinus  = abs(vMinus - vl);              % relative velocity of s/c wrt the moon
vphyper  = sqrt(dvMinus^2 + 2*mu_l/rc_l); % velocity at periapsis of the hyperbola
vc_l     = sqrt(mu_l/rc_l);               % lunar velocity
vMinusInf= abs(vMinus - vl);              % v_inf,Moon Minus
dv2      = vphyper - vc_l;                % delta-v required to park in lunar orbit                                         
dvt      = dv1 + dv2;                     % total delta-v required
phi      = (pi - 1/lPeriod*2*pi*TOF)*180/pi;

%% part (b)
ahyper   = -mu_l/dvMinus^2;
ehyper   = rc_l/abs(ahyper) + 1;
delta    = 2*asind(1/ehyper);
% Resolve quadrant
twoTheta = 2*acosd(1/ehyper);
vPlusInf = sqrt(mu_l/abs(ahyper));        % v_inf,Moon Plus
vPlus    = sqrt(vPlusInf^2 + vl^2 - 2*(vPlusInf)*vl*cosd(delta));
rPlus    = al;
fpaPlus  = asind(vPlusInf*sind(delta)/vPlus);
hPlus    = rPlus*vPlus*cosd(fpaPlus);
EnerPlus = vPlus^2/2 - mu_e/rPlus;
ePlus    = sqrt(1 + 2*EnerPlus*hPlus^2/mu_e^2);
pPlus    = hPlus^2/mu_e;
taPlus   = acosd(pPlus/rPlus/ePlus - 1/ePlus);
aPlus    = pPlus/(1-ePlus^2);
rpPlus   = abs(aPlus)*(ePlus - 1); 
deltaOm  = taMinus - taPlus;

%% part (c) 
dv_eq    = sqrt(vMinusInf^2 + vPlusInf^2 - 2*(vMinusInf)*(vPlusInf)*cosd(delta));
beta     = acosd((vPlus^2 - vaT^2 + dv_eq^2)/-2/vaT/dv_eq); 

veq_vec  = dv_eq*[cosd(alpha);sind(alpha)];


%% part (d) From PS9 Fall 2017
eNew     = (al - rc_e)/(rc_e-al*cosd(TAnew));
aNew     = rc_e/(1-eNew);
ENew     = acos((aNew - al)/(aNew*eNew));
TOF      = sqrt(aNew^3/mu_e)*(ENew-eNew*sin(ENew))/86400;
EnerNew  = -mu_e/2/aNew; 
aTPeriod = 2*pi*sqrt(aNew^3/mu_e);
rMinus   = al;
vMinus   = sqrt(mu_e*(2/al-1/aNew));
fpaMinus = acosd(sqrt(mu_e*aNew*(1-eNew^2))/rMinus/vMinus);
vinfMinus= sqrt(vl^2 + (vMinus)^2 - 2*vl*vMinus*cosd(fpaMinus));

%% part (e)
deltaNew = 2*acosd((vMinus^2 - vl^2 - vinfMinus^2)/(-2*vl*vinfMinus)); 
rPass    = mu_l/(vinfMinus)^2*(1/sind(deltaNew/2)-1);
altPass  = rPass - Rl;
dv_eq    = sqrt(2*vinfMinus^2*(1-cosd(deltaNew)));
alphaNew = -(180-(180 - fpaMinus - 180/2));



ahyper   = -mu_l/vinfMinus^2;
vPlusInf = sqrt(mu_l/abs(ahyper));        % v_inf,Moon Plus
vPlus    = vMinus;
rPlus    = al;
fpaPlus  = -fpaMinus;
taPlus  = atand((rPlus*vPlus^2/mu_e)*cosd(fpaPlus)*sind(fpaPlus)/((rPlus*vPlus^2/mu_e)*cosd(fpaPlus)^2-1));
hPlus    = rPlus*vPlus*cosd(fpaPlus);
pPlus    = hPlus^2/mu_e;
EnerPlus = vPlus^2/2 - mu_e/rPlus;
ePlus    = sqrt(1 + 2*EnerPlus*hPlus^2/mu_e^2);
aPlus    = pPlus/(1-ePlus^2);
aTPeriod = 2*pi*sqrt(aPlus^3/mu_e);
rpPlus   = aPlus*(1-ePlus);

dv_eq    = sqrt(vPlus^2 + vMinus^2 - 2*(vPlus)*(vMinus)*cosd(fpaPlus - fpaMinus));
beta     = acosd((vPlus^2 - vMinus^2 + dv_eq^2)/-2/vMinus/dv_eq); 
alpha    = 180 - beta; 
veq_vec  = dv_eq*[cosd(alpha);sind(alpha)];