function [xrdl, yrdl, zrdl] = eci2rdl2 (reci, veci)

% compute radial frame unit vectors

% input

%  reci = eci position vector (kilometers)
%  veci = eci velocity vector (kilometers/second)

% output

%  xrdl, yrdl, zrdl = radial frame unit vectors

% Orbital Mechanics with MATLAB

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Nr = reci' / norm(reci');

hvec = cross(reci', veci');

Nh = hvec / norm(hvec);

Nt = cross(Nr, Nh);

DCM = [transpose(Nr);transpose(Nt);transpose(Nh)];

out = DCM*reci';

xrdl = out(1);
yrdl = out(2);
zrdl = out(3);


