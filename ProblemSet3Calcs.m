clear all; close all; clc; 

% Problem Set 3 Calculations 

%% Constants
% Jupiter Semi-Major axis =, km
a = 779067093;

% Sun, Jupiter, Trojan Gravitational Parameters km^3/m^2
mu_sun  = 132712200000;
mu_jup  = 126686535; 
mu_tro  = 100;

%% Set up Jupiter-Trojan-Sun configuration 
r21bar  = [a;a];
r21     = norm(r21bar);

r23bar  = [a;0];
r23     = norm(r23bar);

r13bar  = [0;-a];
r13     = norm(r13bar);

r31bar  = -r13bar;
r31     = r13;

r12bar  = -r21bar;
r12     = r21;

r32bar  = -r23bar;
r32     = r23;

%% Calculate all accelerations 
% Sun Dominant Term
a21     = -(mu_sun)*r21bar/r21^3;
norm(a21)

% Jupiter Direct
a13     = (mu_jup)*r13bar/r13^3;
norm(a13)

% Jupiter Indirect
a23     = (mu_jup)*r23bar/r23^3;
norm(a23)

disp('Net Acceleration Due to Jupiter')
norm(a13-a23)

disp('Total Net Acceleration')
norm(a21)+norm(a13)-norm(a23)

% Jupiter Dominant Term
a31     = -(mu_jup)*r31bar/r31^3;

% Sun Direct
a12     = (mu_sun)*r12bar/r12^3;

% Sun Indirect
a32     = (mu_sun)*r32bar/r32^3;

disp('Net Acceleration Due to Sun')
norm(a12-a32)

disp('Total Net Acceleration')
norm(a31)+norm(a12)-norm(a32)
