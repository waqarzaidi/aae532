clear all; close all; clc; format longg;

% Problem Set 11 Calculations 
constants

%% Problem 1 
% Part (a) (i) 
% Jupiter 
jdep  = jday(1991, 11,  6,  0, 0, 0);
jd0   = jday(1973,  4,  6, 12, 0, 0);
jd1   = jday(1974, 12,  3, 12, 0, 0);
jd2   = jday(1979,  9,  1, 12, 0, 0);
jd3   = jday(1990,  2, 23, 12, 0, 0); 
TOF3  = jd3-jd2;
TOF3y = TOF3/365.25;

% Part (b) (ii) 
% Earth
a   = 149597927.0;       
e   = .016712542; 
i   = .001051926*deg2rad;
Om  = 0.0*deg2rad;
om  = 102.914377*deg2rad; 
Mo  = -58.0848626*deg2rad; 
kep = [a;e;i;om;Om;Mo];
t   = (jdep-jd0)*86400;

% Calculate Orbital Period
pEarth   = 2*pi*sqrt(a^3/gSun);

% Advance Mean Anomaly 
n   = sqrt(gSun/kep(1)^3);
Mt  = kep(6) + n*0;

% Calculate True Anomaly (Danby's method)
[~, taEarth] = kepler1 (Mt, kep(2));

r1E = [-143239879.6382527, -43610386.81368236, -800.668087169528]';
v1E = [8.190962234370725, -28.60861087105486, -0.0005252418824888139]';
fpa1E = 90 - 89.04376513694047;

% Jupiter
a   = 778328370.0;       
e   = .048487326; 
i   = 1.30337171*deg2rad;
Om  = 100.435428*deg2rad;
om  = -86.2698735*deg2rad; 
Mo  = 132.596142*deg2rad; 
kep = [a;e;i;om;Om;Mo];

% Calculate Orbital Period
pJupiter   = 2*pi*sqrt(a^3/gSun);

% Advance Mean Anomaly 
n   = sqrt(gSun/kep(1)^3);
Mt  = kep(6) + n*0;

% Calculate True Anomaly (Danby's method)
[~, taJupiter] = kepler1 (Mt, kep(2));

r1J = [734539920.064442, -112607891.6435337, -15971804.7141126]';
v1J = [1.823800906847144, 13.53691378396888, -0.09659472807871161]';
fpa1J = 90 - 91.03369909812101;

% Saturn
a   = 1426990810.0;       
e   = .055571251; 
i   = 2.48804735*deg2rad;
Om  = 113.677893*deg2rad;
om  = -20.6752351*deg2rad; 
Mo  = -142.586443*deg2rad; 
kep = [a;e;i;om;Om;Mo];

% Calculate Orbital Period
pSaturn   = 2*pi*sqrt(a^3/gSun);

% Advance Mean Anomaly 
n   = sqrt(gSun/kep(1)^3);
Mt  = kep(6) + n*0;

% Calculate True Anomaly (Danby's method)
[~, taSaturn] = kepler1 (Mt, kep(2));

r1S = [-1368620398.784393, 299975852.1735503, 49228348.53335983]';
v1S = [-2.594344892687039, -9.461396954525071, 0.268341382729866]';
fpa1S = 90 - 86.97672746148615;

% Neptune
a   = 4496638040.0;       
e   = .008599220; 
i   = 1.77010817*deg2rad;
Om  = 131.780238*deg2rad;
om  = -87.1124809*deg2rad; 
Mo  = -117.599104*deg2rad; 
kep = [a;e;i;om;Om;Mo];

% Calculate Orbital Period
pNeptune   = 2*pi*sqrt(a^3/gSun);

% Advance Mean Anomaly 
n   = sqrt(gSun/kep(1)^3);
Mt  = kep(6) + n*0;

% Calculate True Anomaly (Danby's method)
[~, taNeptune] = kepler1 (Mt, kep(2));

r1N = [980005742.2455396, -4408853376.75942, 68196400.15326667]';
v1N = [5.269125216173574, 1.209490199564219, -0.146332862215686]';
fpa1N = 90 - 132.8555465621446; 

% Part (b) (iii)
% Earth to Jupiter
TOF   = jd1-jd0; 
TA    = atan2d(norm(cross(r1E,r1J)),dot(r1E,r1J));
r1    = norm(r1E); 
r2    = norm(r1J); 
c     = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s     = 1/2*(r1+r2+c);
am    = 1/2*s;
TOFm  = pi*sqrt(am^3/gSun);
TOFp  = 1/3*sqrt(2/gSun)*(s^1.5-(s-c)^1.5);
fun   = @root1d;
save('lamInputs.mat','r1','r2','TA','TOF','gSun');
a     = fsolve(fun,am);
alpha = 2*asind(sqrt(s/2/a));
beta  = 2*asind(sqrt((s-c)/2/a));
p     = (4*a*(s-r1)*(s-r2))/c^2*(sin((alpha*deg2rad+beta*deg2rad)/2)^2);
% p     = (4*a*(s-r1)*(s-r2))/c^2*(sin((alpha*deg2rad-beta*deg2rad)/2)^2);
e     = sqrt(1-p/a);
E     = -gSun/2/a;
P     = 2*pi*sqrt(a^3/gSun);
thetaD = acosd((p-r1)/(r1*e));
thetaA = acosd((p-r2)/(r2*e));
thetaA-thetaD
vD     = sqrt(gSun*(2/r1-1/a));
vA     = sqrt(gSun*(2/r2-1/a));
h      = sqrt(gSun*p);
fpaD   = acosd(h/r1/vD);
fpaA   = acosd(h/r2/vA);
% vinfv  = [vinf*cosd(alphaD);vinf*sind(alphaD);0];
% [v1,v2,iter] = lambert_universal(gSun, 0, r1E, TOF*86400, r1J, 1);
% f and g functions in terms of true anomaly
v     = sqrt(2*(-gSun/2/a+gSun/r1));
r     = r1;
tanom = thetaD*deg2rad; tanom2 = thetaA*deg2rad;
g     = atan(e*sin(tanom)/(1+e*cos(tanom)));
rh    = [cos(tanom);sin(tanom)]*r;
v1    = [sin(g);cos(g)]*v;
v2    = v1(1)*[cos(tanom);sin(tanom)];
v3    = v1(2)*[-sin(tanom);cos(tanom)];
vh    = v2+v3;
f     = 1-r2/p*(1-cos(tanom2-tanom));
g     = r*r2/sqrt(gSun*p)*sin(tanom2-tanom); 
fdot  = dot(rh,vh)/p/r*(1-cos(tanom2-tanom))-1/r*sqrt(gSun/p)*sin(tanom2-tanom);
gdot  = 1-r/p*(1-cos(tanom2-tanom));
v1    = (r1J - f*r1E)/g;
vdEJ  = v1;
v2    = fdot*r1E + gdot*v1;
vAEJ  = v2;
coe   = coe_from_sv(r1E,v1,gSun);
vE     = sqrt(gSun/r1);
% vE    = norm(v1E);
vinfv  = v1 - v1E; 
vinf  = norm(vinfv);
% vinfv  = v1 - v1E;
dv     = sqrt(vinf^2 + 2*gEarth/(300+rEarth)) - sqrt(gEarth/(300+rEarth));
% vinf  = norm(vinfv);

% plots 
% %% David's code
% figure()
% t = linspace (0,2*pi);
% t3 = linspace (thetaD*deg2rad,thetaA*deg2rad);
% t4 = linspace (0.60,-2.892);
% Earth_SM = aEarth; % 1.496e8;
% Jupiter_SM = aJupiter; %2.2795e8;
% a_min = a; %1.874e8;
% e_min = e; %0.226;
% ae = a_min*e_min;
% b_min = a_min*((1-(e_min^2))^0.5);
% rt1= (-34.4/57.3);
% y_correction = sin(abs(rt1))*(ae);
% osc = (((ae^2)+(ae^2))-(2*ae*ae*cos(rt1)))^0.5;
% n = acos((y_correction)/(osc));
% x_correction = sin(n)*osc;
% x_earth = 0;
% y_earth = 0;
% x = a_min*cos(t3)*cos(rt1) - b_min*sin(t3)*sin(rt1)-(a_min*e_min)+ x_correction;
% y = a_min*cos(t3)*sin(rt1) + b_min*sin(t3)*cos(rt1)+ y_correction;
% x7 = a_min*cos(t4)*cos(rt1) - b_min*sin(t4)*sin(rt1)-(a_min*e_min)+ x_correction;
% y7 = a_min*cos(t4)*sin(rt1) + b_min*sin(t4)*cos(rt1)+ y_correction;
% x5 = aEarth*cos(t); % Earth_SM*cos(t);
% y5 = aEarth*sin(t); % Earth_SM*sin(t);
% x6 = aJupiter*cos(t); % Mars_SM*cos(t);
% y6 = aJupiter*sin(t); % Mars_SM*sin(t);
% t2 = linspace (-1.5,1.5);
% rt = (90/57.3);
% x2 = (1*cosh(t2)*cos(rt))-(1*sinh(t2)*sin(rt));
% x21 = (-1*cosh(t2)*cos(rt))+(1*sinh(t2)*sin(rt));
% y2 = (1*cosh(t2)*sin(rt))+(1*sinh(t2)*cos(rt));
% y21 = (-1*cosh(t2)*sin(rt))+(-1*sinh(t2)*cos(rt));
% plot (x,y,'g-','LineWidth',2)
% hold on
% plot (x5,y5,'b-','LineWidth',2)
% hold on
% plot (x6,y6,'r-','LineWidth',2)
% hold on
% % plot (x7,y7,'k-','LineWidth',2)
% % hold on
% % plot (x_earth,y_earth,'r*')
% [x,y,z] = sphere(20);
% surf(250*rSaturn*x,250*rSaturn*y,250*rSaturn*z); hold on;
% axis equal
% xlabel('ehat, km'); ylabel('phat, km'); grid on;
% legend('Transfer Arc','Earth','Jupiter')
% grid minor 

% Jupiter to Saturn 
TOF   = jd2-jd1;
TA    = atan2d(norm(cross(r1J,r1S)),dot(r1J,r1S));
r1    = norm(r1J); 
r2    = norm(r1S); 
c     = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s     = 1/2*(r1+r2+c);
am    = 1/2*s;
TOFm  = pi*sqrt(am^3/gSun);
TOFp  = 1/3*sqrt(2/gSun)*(s^1.5-(s-c)^1.5);
fun   = @root1d;
save('lamInputs.mat','r1','r2','TA','TOF','gSun');
a     = fsolve(fun,am);
alpha = 2*asind(sqrt(s/2/a));
beta  = 2*asind(sqrt((s-c)/2/a));
p     = (4*a*(s-r1)*(s-r2))/c^2*(sin((alpha*deg2rad+beta*deg2rad)/2)^2);
% p     = (4*a*(s-r1)*(s-r2))/c^2*(sin((alpha*deg2rad-beta*deg2rad)/2)^2);
e     = sqrt(1-p/a);
E     = -gSun/2/a;
P     = 2*pi*sqrt(a^3/gSun);
thetaD = -acosd((p-r1)/(r1*e));
thetaA = acosd((p-r2)/(r2*e));
thetaA-thetaD
vD     = sqrt(gSun*(2/r1-1/a));
vA     = sqrt(gSun*(2/r2-1/a));
h      = sqrt(gSun*p);
fpaD   = -acosd(h/r1/vD);
fpaA   = acosd(h/r2/vA);
% [v1,v2,iter] = lambert_universal(gSun, 0, r1E, TOF*86400, r1J, 1);
% f and g functions in terms of true anomaly
v     = sqrt(2*(-gSun/2/a+gSun/r1));
r     = r1;
tanom = thetaD*deg2rad; tanom2 = thetaA*deg2rad;
g     = atan(e*sin(tanom)/(1+e*cos(tanom)));
rh    = [cos(tanom);sin(tanom)]*r;
v1    = [sin(g);cos(g)]*v;
v2    = v1(1)*[cos(tanom);sin(tanom)];
v3    = v1(2)*[-sin(tanom);cos(tanom)];
vh    = v2+v3;
f     = 1-r2/p*(1-cos(tanom2-tanom));
g     = r*r2/sqrt(gSun*p)*sin(tanom2-tanom); 
fdot  = dot(rh,vh)/p/r*(1-cos(tanom2-tanom))-1/r*sqrt(gSun/p)*sin(tanom2-tanom);
gdot  = 1-r/p*(1-cos(tanom2-tanom));
v1    = (r1S - f*r1J)/g;
vDJS  = v1;
v2    = fdot*r1J + gdot*v1;
vAJS  = v2;
coe   = coe_from_sv(r1J,v1,gSun);

figure()
t = linspace (0,2*pi);
t3 = linspace ((thetaD+37)*deg2rad,(thetaA-50)*deg2rad); %0,360*deg2rad); %
t4 = linspace (0.60,-2.892);
Jupiter_SM = aJupiter; % 1.496e8;
Saturn_SM = aSaturn; %2.2795e8;
a_min = a; %1.874e8;
e_min = e; %0.226;
ae = a_min*e_min;
b_min = a_min*((1-(e_min^2))^0.5);
rt1= (-34.4/57.3);
y_correction = sin(abs(rt1))*(ae);
osc = (((ae^2)+(ae^2))-(2*ae*ae*cos(rt1)))^0.5;
n = acos((y_correction)/(osc));
x_correction = sin(n)*osc;
x_earth = 0;
y_earth = 0;
x = a_min*cos(t3)*cos(rt1) - b_min*sin(t3)*sin(rt1)-(a_min*e_min)+ x_correction;
y = a_min*cos(t3)*sin(rt1) + b_min*sin(t3)*cos(rt1)+ y_correction;
x7 = a_min*cos(t4)*cos(rt1) - b_min*sin(t4)*sin(rt1)-(a_min*e_min)+ x_correction;
y7 = a_min*cos(t4)*sin(rt1) + b_min*sin(t4)*cos(rt1)+ y_correction;
x5 = aJupiter*cos(t); % Earth_SM*cos(t);
y5 = aJupiter*sin(t); % Earth_SM*sin(t);
x6 = aSaturn*cos(t); % Mars_SM*cos(t);
y6 = aSaturn*sin(t); % Mars_SM*sin(t);
t2 = linspace (-1.5,1.5);
rt = (90/57.3);
x2 = (1*cosh(t2)*cos(rt))-(1*sinh(t2)*sin(rt));
x21 = (-1*cosh(t2)*cos(rt))+(1*sinh(t2)*sin(rt));
y2 = (1*cosh(t2)*sin(rt))+(1*sinh(t2)*cos(rt));
y21 = (-1*cosh(t2)*sin(rt))+(-1*sinh(t2)*cos(rt));
plot (x,y,'g-','LineWidth',2)
hold on
plot (x5,y5,'b-','LineWidth',2)
hold on
plot (x6,y6,'r-','LineWidth',2)
hold on
% plot (x7,y7,'k-','LineWidth',2)
% hold on
% plot (x_earth,y_earth,'r*')
[x,y,z] = sphere(20);
surf(250*rSaturn*x,250*rSaturn*y,250*rSaturn*z); hold on;
axis equal
xlabel('ehat, km'); ylabel('phat, km'); grid on;
legend('Transfer Arc','Jupiter','Saturn')
grid minor 



dveq  = vDJS - vAEJ;
vinfm = vAEJ - v1J;
vinfp = vDJS - v1J;
vinfa = (norm(vinfp)+norm(vinfm))/2; 
delta = acosd(dot(vinfm,vinfp)/(norm(vinfm)*norm(vinfp)));
ahyp  = gJupiter/vinfa^2;
ehyp  = 1/sind(delta/2);
rphyp = abs(ahyp)*(ehyp-1);
ralt  = rphyp - rJupiter;

% Compute a vector with positions

n = size(TA,2);

for i = 1:n
    r(i) = ahyp*(ehyp^2-1)/(1+ehyp*cos(TA(1,i)*pi/180));
end

for i = 1:n
    v(i) = sqrt(2*(gJupiter/2/ahyp+gJupiter/r(i)));
end

for i = 1:n
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
end

for i = 1:n
    e_v(i) = -v(i)*sin(TA(i)*pi/180);
    theta_v(i) = v(i)*cos(TA(i)*pi/180);
end

ind = find(e_r(1,:) < 0.1e7);

% figure(1)
% plot(e_r(1,:),theta_r(1,:),'k-');
% hold on
% plot(0,0,'o');
% hold on
% plot(e_r(1,ind),theta_r(1,ind),'r-');
% xlim([-1000000 2000000]);
% grid on; grid minor; 
% ylabel('$\hat{p}$, km','Interpreter','latex')
% xlabel('$\hat{e}$, km','Interpreter','latex')

vinfm = vAJS - v1S; 
vinf  = norm(vinfm);
rhyp = 20900 + rSaturn;
ahyp = gSaturn/vinf^2;
ehyp = rhyp/ahyp + 1;
delta = 2*asind(1/ehyp);

% Compute a vector with positions
TA = linspace(0,360,100);
n = size(TA,2);

for i = 1:n
    r(i) = ahyp*(ehyp^2-1)/(1+ehyp*cos(TA(1,i)*pi/180));
end

for i = 1:n
    v(i) = sqrt(2*(gJupiter/2/ahyp+gJupiter/r(i)));
end

for i = 1:n
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
end

for i = 1:n
    e_v(i) = -v(i)*sin(TA(i)*pi/180);
    theta_v(i) = v(i)*cos(TA(i)*pi/180);
end

ind = find(e_r(1,:) < 0.1e7);

% figure(2)
% plot(e_r(1,:),theta_r(1,:),'k-');
% hold on
% plot(0,0,'o');
% hold on
% plot(e_r(1,ind),theta_r(1,ind),'r-');
% xlim([-1000000 2000000]);
% grid on; grid minor; 
% ylabel('$\hat{p}$, km','Interpreter','latex')
% xlabel('$\hat{e}$, km','Interpreter','latex')

% Saturn to Neptune 
TOF   = jd3-jd2;
TA    = atan2d(norm(cross(r1S,r1N)),dot(r1S,r1N));
r1    = norm(r1S); 
r2    = norm(r1N); 
c     = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s     = 1/2*(r1+r2+c);
am    = 1/2*s;
TOFm  = pi*sqrt(am^3/gSun);
TOFp  = 1/3*sqrt(2/gSun)*(s^1.5-(s-c)^1.5);
fun   = @root1hd;
save('lamInputs.mat','r1','r2','TA','TOF','gSun');
a     = fsolve(fun,am);
alpha = 2*asind(sqrt(s/2/a));
beta  = 2*asind(sqrt((s-c)/2/a));
p     = (4*a*(s-r1)*(s-r2))/c^2*(sin((alpha*deg2rad+beta*deg2rad)/2)^2);
% p     = (4*a*(s-r1)*(s-r2))/c^2*(sin((alpha*deg2rad-beta*deg2rad)/2)^2);
ahyp  = a;
ehyp  = 1/sind(delta/2);
rphyp = abs(ahyp)*(ehyp-1);
vD     = sqrt(gSun*(2/r1-1/ahyp));

[v1,v2,iter] = lambert_universal(gSun, 0, r1S, TOF*86400, r1N, 0); %[aNeptune;0;0], 0);

dv3 = v1 - vAJS;

% Compute a vector with positions
TA = linspace(0,360,100);
n = size(TA,2);

for i = 1:n
    r(i) = ahyp*(ehyp^2-1)/(1+ehyp*cos(TA(1,i)*pi/180));
end

for i = 1:n
    v(i) = sqrt(2*(gSaturn/2/ahyp+gSaturn/r(i)));
end

for i = 1:n
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
end

for i = 1:n
    e_v(i) = -v(i)*sin(TA(i)*pi/180);
    theta_v(i) = v(i)*cos(TA(i)*pi/180);
end

ind = find(e_r(1,:) < 0.1e7);

figure(2)
plot(e_r(1,:),theta_r(1,:),'k-');
hold on
plot(0,0,'o');
% hold on
% plot(e_r(1,ind),theta_r(1,ind),'r-');
% xlim([-1000000 2000000]);
grid on; grid minor; 
ylabel('$\hat{p}$, km','Interpreter','latex')
xlabel('$\hat{e}$, km','Interpreter','latex')


figure()
t = linspace (0,2*pi);
t3 = linspace (0,360*deg2rad); %0,360*deg2rad); %
t4 = linspace (0.60,-2.892);
Saturn_SM = aSaturn; % 1.496e8;
Neptune_SM = aNeptune; %2.2795e8;
a_min = ahyp; %1.874e8;
e_min = ehyp; %0.226;
ae = a_min*e_min;
b_min = a_min*((1-(e_min^2))^0.5);
rt1= (-34.4/57.3);
y_correction = sin(abs(rt1))*(ae);
osc = (((ae^2)+(ae^2))-(2*ae*ae*cos(rt1)))^0.5;
n = acos((y_correction)/(osc));
x_correction = sin(n)*osc;
x_earth = 0;
y_earth = 0;
x = a_min*cos(t3)*cos(rt1) - b_min*sin(t3)*sin(rt1)-(a_min*e_min)+ x_correction;
y = a_min*cos(t3)*sin(rt1) + b_min*sin(t3)*cos(rt1)+ y_correction;
x7 = a_min*cos(t4)*cos(rt1) - b_min*sin(t4)*sin(rt1)-(a_min*e_min)+ x_correction;
y7 = a_min*cos(t4)*sin(rt1) + b_min*sin(t4)*cos(rt1)+ y_correction;
x5 = aSaturn*cos(t); % Earth_SM*cos(t);
y5 = aSaturn*sin(t); % Earth_SM*sin(t);
x6 = aNeptune*cos(t); % Mars_SM*cos(t);
y6 = aNeptune*sin(t); % Mars_SM*sin(t);
t2 = linspace (-1.5,1.5);
rt = (90/57.3);
x2 = (1*cosh(t2)*cos(rt))-(1*sinh(t2)*sin(rt));
x21 = (-1*cosh(t2)*cos(rt))+(1*sinh(t2)*sin(rt));
y2 = (1*cosh(t2)*sin(rt))+(1*sinh(t2)*cos(rt));
y21 = (-1*cosh(t2)*sin(rt))+(-1*sinh(t2)*cos(rt));
plot (x,y,'g-','LineWidth',2)
hold on
plot (x5,y5,'b-','LineWidth',2)
hold on
plot (x6,y6,'r-','LineWidth',2)
hold on
% plot (x7,y7,'k-','LineWidth',2)
% hold on
% plot (x_earth,y_earth,'r*')
[x,y,z] = sphere(20);
surf(250*rSaturn*x,250*rSaturn*y,250*rSaturn*z); hold on;
axis equal
xlabel('ehat, km'); ylabel('phat, km'); grid on;
legend('Transfer Arc','Jupiter','Saturn')
grid minor 


function F = root1d(x)
load lamInputs.mat
c      = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s      = 1/2*(r1+r2+c);
alpham = 2*asin(sqrt(s/2/x));
betam  = 2*asin(sqrt((s-c)/2/x));
alphan = alpham;
betan  = betam;
F = sqrt(x^3/gSun)*(alphan-betan-sin(alphan)+sin(betan)) - TOF*86400;
end

function F = root1hd(x)
load lamInputs.mat
c      = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s      = 1/2*(r1+r2+c);
alpham = 2*asin(sqrt(s/2/x));
betam  = 2*asin(sqrt((s-c)/2/x));
alphan = alpham;
betan  = betam;
F = sqrt(x^3/gSun)*(sinh(alphan)-alphan-sinh(betan)+betan) - TOF*86400;
end
