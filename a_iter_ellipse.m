
%%% TOF in seconds
%%% a_init in km
%%% mu in km^3/s^2

function [a_res,TOF_res,alpha_f,beta_f,fval] = a_iter_ellipse(a_init, s,c,TOF,mu,type_n,type_l)

options = optimoptions('fsolve','MaxFunctionEvaluations',1000);



%% Type 2B
if type_n == 2 && type_l == 'B'
    % Only for type 2B
    % TOF = TOF*3600;
    
    
    % TOF_2B_f = TOF - sqrt(aa^3 / mu_earth) * ((alpha_f - sin(alpha_f)) - (beta_f - sin(beta_f)) );
    TOF_2B_f = @(a) TOF - sqrt(a^3 / mu) * ((2*pi - 2 * asin( sqrt(s/(2*a)) ) - sin(2*pi - 2 * asin( sqrt(s/(2*a)) )))...
        - (-2 * asin( sqrt( (s-c)/(2*a) ) ) - sin(-2 * asin( sqrt( (s-c)/(2*a) ) ))) );
    % TOF_2B_hours = TOF_2B./3600
    
    [a_res,fval] = fsolve(TOF_2B_f,a_init,options);
    
    
    alpha_f = 2*pi - 2 * asin( sqrt(s/(2*a_res)) );
    beta_f = -2 * asin( sqrt( (s-c)/(2*a_res) ) );
    
    TOF_res = sqrt(a_res^3 / mu) * ((alpha_f - sin(alpha_f)) - (beta_f - sin(beta_f)) );
    %  TOF_res = TOF_res/3600;
    
    %% Type 2A
elseif type_n == 2 && type_l == 'A'
    
    
    TOF_2A_f = @(a) TOF - sqrt(a^3 / mu) * (( 2 * asin( sqrt(s/(2*a)) ) - sin( 2 * asin( sqrt(s/(2*a)) )))...
        - (-2 * asin( sqrt( (s-c)/(2*a) ) ) - sin(-2 * asin( sqrt( (s-c)/(2*a) ) ))) );
    % TOF_2B_hours = TOF_2B./3600
    
    [a_res,fval] = fsolve(TOF_2A_f,a_init,options);
    
    alpha_f = 2 * asin( sqrt(s/(2*a_res)) );
    beta_f = -2 * asin( sqrt( (s-c)/(2*a_res) ) );
    TOF_res = sqrt(a_res^3 / mu) * ((alpha_f - sin(alpha_f)) - (beta_f - sin(beta_f)) );
    %  TOF_res = TOF_res/3600;
    
    %% Type 1A
elseif type_n == 1 && type_l =='A'
    
    TOF_1A_f = @(a) TOF - sqrt(a^3 / mu) * (( 2 * asin( sqrt(s/(2*a)) ) - sin( 2 * asin( sqrt(s/(2*a)) )))...
        - (2 * asin( sqrt( (s-c)/(2*a) ) ) - sin(2 * asin( sqrt( (s-c)/(2*a) ) ))) );
    % TOF_2B_hours = TOF_2B./3600
    
    [a_res,fval] = fsolve(TOF_1A_f,a_init,options);
    
    if abs(fval)>0.1
       [a_res,fval]= fsolve(TOF_1A_f,a_res);
    end
        
    alpha_f = 2 * asin( sqrt(s/(2*a_res)) );
    beta_f = 2 * asin( sqrt( (s-c)/(2*a_res) ) );
    
    TOF_res = sqrt(a_res^3 / mu) * ((alpha_f - sin(alpha_f)) - (beta_f - sin(beta_f)) );
    %  TOF_res = TOF_res/3600;
    
    %% Type 1B
elseif type_n == 1 && type_l == 'B'
    
    TOF_1B_f = @(a) TOF - sqrt(a^3 / mu) * ((2*pi - 2 * asin( sqrt(s/(2*a)) ) - sin(2*pi - 2 * asin( sqrt(s/(2*a)) )))...
        - (2 * asin( sqrt( (s-c)/(2*a) ) ) - sin(2 * asin( sqrt( (s-c)/(2*a) ) ))) );
    % TOF_2B_hours = TOF_2B./3600
    
    [a_res,fval] = fsolve(TOF_1B_f,a_init,options);
    
    alpha_f = 2*pi - 2 * asin( sqrt(s/(2*a_res)) );
    beta_f = 2 * asin( sqrt( (s-c)/(2*a_res) ) );
    
    TOF_res = sqrt(a_res^3 / mu) * ((alpha_f - sin(alpha_f)) - (beta_f - sin(beta_f)) );
    %  TOF_res = TOF_res/3600;
    
    
    
end



