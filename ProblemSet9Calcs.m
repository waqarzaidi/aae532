clear all; close all; clc; format longg;

% Problem Set 9 Calculations 
constants

%% Problem 1 (from PS8 Fall 2015 Problem 3)
% Given
Alt    = 100; % 150; %
dInc   = 90;
rA     = 17000; %25*rLuna; % 15000; %

% part (a) (i)
rC     = rLuna + Alt;
vC     = sqrt(gLuna/rC); 
dvInc  = 2*vC*sind(dInc/2);

% part (a) (ii)
aT     = 0.5*(rC + rA);
nT     = 1/sqrt(aT^3/gLuna);
TOF    = 2*pi/nT;
vP     = sqrt(2*gLuna/rC-gLuna/aT);
dv1    = vP - vC; 
beta1  = 0;
alpha1 = 0;
dv1Vec = dv1*[cosd(beta1)*cosd(alpha1);cosd(beta1)*sind(alpha1);sind(beta1)];
vA     = sqrt(2*gLuna/rA-gLuna/aT);
dv2    = 2*vA*sind(dInc/2);
beta2  = 135;
alpha2 = 0;
dv2Vec = dv2*[cosd(beta2)*cosd(alpha2);cosd(beta2)*sind(alpha2);sind(beta2)];
dv3    = vC - vP; 
beta3  = 0;
alpha3 = 0;
dv3Vec = dv3*[cosd(beta3)*cosd(alpha3);cosd(beta3)*sind(alpha3);sind(beta3)];
total  = dv1 + dv2 + abs(dv3);

%% Problem 2 (from PS9 Fall 2017 Problem 4)
% Given
rP       = 8*rSaturn;
rA       = 32*rSaturn;
eTitan   = 0.2;
aTitan   = 20*rSaturn;

% part (a) 
pTitan   = aTitan*(1-eTitan^2);
rPTitan  = aTitan*(1-eTitan);
rATitan  = aTitan*(1+eTitan);
a        = (rP + rA)/2;
e        = (rA - rP)/(rA + rP); 
p        = a*(1-e^2);
taMinus  = acosd((pTitan-p)/(p*eTitan-e*pTitan));
rMinus   = p/(1+e*cosd(taMinus));
vMinus   = sqrt(2*(gSaturn/rMinus - gSaturn/2/a));
fpaMinus = atand(e*sind(taMinus)/(1+e*cosd(taMinus)));
fpaTitan = atand(eTitan*sind(taMinus)/(1+eTitan*cosd(taMinus)));
vTitan   = vMinus;
vInfMinus = sqrt(vMinus^2 + vMinus^2 - 2*vMinus*vMinus*cosd(fpaMinus-fpaTitan));
ahyper   = -gTitan/vInfMinus^2;
ehyper   = (1000+rTitan)/abs(ahyper)+1;
delta    = 2*asind(1/ehyper);
vInfPlus = vInfMinus;
DVeq     = sqrt(vInfMinus^2+vInfPlus^2-2*vInfMinus*vInfPlus*cosd(delta));
eta      = acosd((vInfMinus^2+vTitan^2-vMinus^2)/(2*vInfMinus*vTitan));
vPlus    = sqrt(vInfMinus^2+vTitan^2-2*vInfMinus*vTitan*cosd(eta+delta));
epsilon  = acosd((vMinus^2+DVeq^2-vPlus^2)/(2*vMinus*DVeq));
alpha    = -(180 - epsilon);
rPlus    = rMinus;
fpaPlus  = acosd((vPlus^2 + vTitan^2 - vInfPlus^2)/(2*vPlus*vTitan))+fpaTitan;
num      = (rMinus*vPlus^2)*cosd(fpaPlus)*sind(fpaPlus)/gSaturn;
den      = (rMinus*vPlus^2/gSaturn)*cosd(fpaPlus)^2-1;
taPlus   = mod(atan2(num,den)*rad2deg,360);
ePlus    = sqrt((rPlus*vPlus^2/gSaturn-1)^2*cosd(fpaPlus)^2+sind(fpaPlus)^2);
hPlus    = cosd(fpaPlus)*rPlus*vPlus;
% pPlus    = hPlus^2/gSaturn;
pPlus    = rPlus*(1+ePlus*cosd(taPlus));
aPlus    = pPlus/(1-ePlus^2);
PPlus    = 2*pi*sqrt(aPlus^3/gSaturn)/86400;
rpPlus   = aPlus*(1-ePlus);
raPlus   = aPlus*(1+ePlus);
deltaOm  = taMinus - taPlus;
EnerPlus = -gSaturn/2/aPlus;

eIapetus = 0.0286125;
aIapetus = 3560820; 
pIapetus = aIapetus*(1-eIapetus^2);


eHyperion = 0.1230061;
aHyperion = 1481009; 
pHyperion = aHyperion*(1-eHyperion^2);

p1    = p; p2 = pPlus; p3 = pTitan; 
e1    = e; e2 = ePlus; e3 = eTitan; 
TA    = linspace(0,360,100);

for i = 1:size(TA,2)
    r1(i) = p1/(1+e1*cosd(TA(1,i)));
    r2(i) = p2/(1+e2*cosd(TA(1,i)));
    r3(i) = p3/(1+e3*cosd(TA(1,i)));    
    r4(i) = pIapetus/(1+eIapetus*cosd(TA(1,i)));  
    r5(i) = pHyperion/(1+eHyperion*cosd(TA(1,i))); 
end

for i = 1:size(TA,2)
    e_r1(i) = r1(i)*cosd(TA(i));
    theta_r1(i) = r1(i)*sind(TA(i));    
    e_r2(i) = r2(i)*cosd(TA(i)+deltaOm);
    theta_r2(i) = r2(i)*sind(TA(i)+deltaOm);
    e_r3(i) = r3(i)*cosd(TA(i));
    theta_r3(i) = r3(i)*sind(TA(i));    
    e_r4(i) = r4(i)*cosd(TA(i));
    theta_r4(i) = r4(i)*sind(TA(i));  
    e_r5(i) = r5(i)*cosd(TA(i));
    theta_r5(i) = r5(i)*sind(TA(i));      
end
figure(2)
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(e_r2,theta_r2,'r-','LineWidth',2); hold on;
plot(e_r3,theta_r3,'k-','LineWidth',2); hold on;
plot(e_r4,theta_r4,'g-','LineWidth',2); hold on;
plot(e_r5,theta_r5,'m-','LineWidth',2); hold on;
% plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',7);
[x,y,z] = sphere(20);
surf(4*rSaturn*x,4*rSaturn*y,4*rSaturn*z); hold on;
axis equal
xlabel('ehat, km'); ylabel('phat, km'); grid on;
legend('Original','Post-S wingby','Titan','Iapetus','Hyperion')

p1    = p; p2 = ahyper*(1-ehyper^2);
e1    = e; e2 = ehyper;
TA    = linspace(-90,90,100);

for i = 1:size(TA,2)
    r1(i) = p1/(1+e1*cosd(TA(1,i)));
    r2(i) = p2/(1+e2*cosd(TA(1,i)));
end

for i = 1:size(TA,2)
    e_r1(i) = r1(i)*cosd(TA(i));
    theta_r1(i) = r1(i)*sind(TA(i));    
    e_r2(i) = r2(i)*cosd(TA(i));
    theta_r2(i) = r2(i)*sind(TA(i));
    
end
figure(3)
% plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(e_r2,theta_r2,'r-','LineWidth',2); hold on;
% plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',7);
[x,y,z] = sphere(20);
surf(rTitan*x,rTitan*y,rTitan*z); hold on;
axis equal
xlabel('ehat, km'); ylabel('phat, km'); grid on;

%% Problem 3 (from PS9 Fall 2016 Problem 1)
% Given 
vFlyby = 4*rVenus; 
% vFlyby = 2990; 

% part (a) 
jd1  = jday(2004, 8, 3, 0, 0, 0);
jd2  = jday(2006,10,24, 0, 0, 0);
TOF1 = jd2-jd1; 

jd3  = jday(2008, 1,14, 0, 0, 0);
TOF2 = jd3-jd1; 

jd4  = jday(2011, 3,18, 0, 0, 0);
TOF3 = jd4-jd1;  

% part (b) 
% aEarth = 149597870; aMercury = 57910000; aVenus = 108209475; 
aT        = (aEarth + aMercury)/2; 
eT        = (aEarth - aMercury)/(aEarth + aMercury); 
pMinus    = aT*(1-eT^2);
taMinus   = 360-acosd(aT*(1-eT^2)/aVenus/eT-1/eT);
EMinus    = 2*atan(((1+eT)/(1-eT))^(-0.5)*tand(taMinus/2));
TOF1      = (mod((EMinus - eT*sin(EMinus)),2*pi)-pi)*sqrt(aT^3/gSun);
frac      =  vFlyby/rVenus;

% part (c)
fpaMinus  = atand(eT*sind(taMinus)/(1+eT*cosd(taMinus)));
vMinus    = sqrt(2*(gSun/aVenus-gSun/2/aT)); 
vVenus    = sqrt(gSun/aVenus);
vInfMinus = sqrt(vMinus^2 + vVenus^2 - 2*vMinus*vVenus*cosd(fpaMinus)); 
vInfPlus  = vInfMinus;
Ehyper    = vInfMinus^2/2;
ahyperH   = gVenus/2/Ehyper;
ahyper    = -gVenus/vInfMinus^2;
ehyper    = (vFlyby)/abs(ahyper)+1;
delta     = 2*asind(1/ehyper);
angleA    = -asind(vMinus*sind(fpaMinus)/vInfMinus);
angleB    = angleA - delta;
vPlus     = sqrt(vVenus^2 + vInfPlus^2 - 2*vVenus*vInfPlus*cosd(angleB));
fpaPlus   = -asind(vInfPlus*sind(angleB)/vPlus);
num       = ((aVenus+vFlyby)*vPlus^2)*cosd(fpaPlus)*sind(fpaPlus)/gSun;
den       = ((aVenus+vFlyby)*vPlus^2/gSun)*cosd(fpaPlus)^2-1;
taPlus    = mod(atan2(num,den)*rad2deg,360);
ePlus     = sqrt((aVenus*vPlus^2/gSun-1)^2*cosd(fpaPlus)^2+sind(fpaPlus)^2);
pPlus     = aVenus*(1+ePlus*cosd(taPlus));
aPlus     = -gSun/(vPlus^2 - 2*gSun/aVenus); %pPlus/(1-ePlus^2);
PPlus     = 2*pi*sqrt(aPlus^3/gSun);
rpPlus    = aPlus*(1-ePlus);
raPlus    = aPlus*(1+ePlus);
deltaOm   = taMinus - taPlus;

p1    = pMinus; p2 = pPlus; p3 = aMercury*(1-eMercury^2); p4 = aVenus*(1-eVenus^2); p5 = aEarth*(1-eEarth^2);
e1    = eT; e2 = ePlus;  
TA   = linspace(0,360,100);
TA1   = linspace(0,360,100);

for i = 1:size(TA1,2)
    r1(i) = p1/(1+e1*cosd(TA1(1,i)));
    r2(i) = p2/(1+e2*cosd(TA1(1,i)));
end

for i = 1:size(TA,2)
    r3(i) = p3/(1+eMercury*cosd(TA(1,i)));
    r4(i) = p4/(1+eVenus*cosd(TA(1,i)));
    r5(i) = p5/(1+eEarth*cosd(TA(1,i)));
end

for i = 1:size(TA1,2)
    e_r1(i) = r1(i)*cosd(TA1(i));
    theta_r1(i) = r1(i)*sind(TA1(i));    
    e_r2(i) = r2(i)*cosd(TA1(i)+deltaOm);
    theta_r2(i) = r2(i)*sind(TA1(i)+deltaOm);   
end

for i = 1:size(TA,2)
    e_r3(i) = r3(i)*cosd(TA(i));
    theta_r3(i) = r3(i)*sind(TA(i));    
    e_r4(i) = r4(i)*cosd(TA(i));
    theta_r4(i) = r4(i)*sind(TA(i));   
    e_r5(i) = r5(i)*cosd(TA(i));
    theta_r5(i) = r5(i)*sind(TA(i));      
end

figure(6)
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(e_r2,theta_r2,'r-','LineWidth',2); hold on;
plot(e_r3,theta_r3,'g-','LineWidth',2); hold on;
plot(e_r4,theta_r4,'m-','LineWidth',2); hold on;
plot(e_r5,theta_r5,'y-','LineWidth',2); hold on;
plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',14);
plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',4,'MarkerFaceColor','k');
legend('Original','Post Venus Gravity Assist','Mercury','Venus','Earth')
axis equal
xlabel('ehat, km'); ylabel('phat, km'); grid on;

p1    = pMinus; p2 = pPlus; 
e1    = eT; e2 = ePlus;  
TA    = linspace(0,360,100);

for i = 1:size(TA,2)
    r1(i) = p1/(1+e1*cosd(TA(1,i)));
    r2(i) = p2/(1+e2*cosd(TA(1,i)));
end

for i = 1:size(TA,2)
    e_r1(i) = r1(i)*cosd(TA(i));
    theta_r1(i) = r1(i)*sind(TA(i));    
    e_r2(i) = r2(i)*cosd(TA(i)+deltaOm);
    theta_r2(i) = r2(i)*sind(TA(i)+deltaOm);   
end
figure(5)
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(e_r2,theta_r2,'r-','LineWidth',2); hold on;
plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',14);
plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',4,'MarkerFaceColor','k');
legend('Original','Post Venus Gravity Assist')
axis equal
xlabel('ehat, km'); ylabel('phat, km'); grid on;


% part (d) 
DVeq      = sqrt(vInfMinus^2+vInfPlus^2-2*vInfMinus*vInfPlus*cosd(delta));
phi       = asind(sind(abs(fpaPlus-fpaMinus))*vPlus/DVeq);
alpha     = 180-phi;
DVeqVec   = DVeq*[cosd(alpha);sind(alpha)]; 

% part (e) 
E1  = 2*atan(((1+ePlus)/(1-ePlus))^(-0.5)*tand(taPlus/2));
taMercury = -acosd((aPlus*(1-ePlus^2)/aMercury-1)*(1/ePlus));
E2  = 2*atan(((1+ePlus)/(1-ePlus))^(-0.5)*tand(taMercury/2));

% E1        = -acos((aT-aVenus)/aT/eT);
% E2        = -acos((aT-aMercury)/aT/eT);
TOF2      = sqrt(aT^3/gSun)*(E2-E1-eT*(sin(E2)-sin(E1)));

% part (f) 
nVenus    = sqrt(gSun/aVenus^3);
nMercury  = sqrt(gSun/aMercury^3);
PVenus    = 2*pi*sqrt(aVenus^3/gSun);
PMercury  = 2*pi*sqrt(aMercury^3/gSun); 

revsV     = TOF1/PVenus;
revsM     = (TOF1+TOF2)/PMercury; 

phi1      = ((taMinus*deg2rad-pi) - sqrt(gSun/aVenus^3)*TOF1)*rad2deg+360;
phi2      = ((taMinus*deg2rad-pi) + (taMercury*deg2rad-taPlus*deg2rad) - nMercury*(revsM-1))*rad2deg+360;

phi1check = (nVenus*(TOF1) - taPlus*deg2rad)*rad2deg+360
phi2check = (nMercury*(TOF1+TOF2) - taPlus*deg2rad - taMinus*deg2rad)*rad2deg+360