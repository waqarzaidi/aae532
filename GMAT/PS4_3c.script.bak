%General Mission Analysis Tool(GMAT) Script
%Created: 2018-09-21 11:02:26


%----------------------------------------
%---------- Spacecraft
%----------------------------------------

Create Spacecraft Sat;
GMAT Sat.DateFormat = UTCGregorian;
GMAT Sat.Epoch = '18 Sep 2018 00:00:00.000';
GMAT Sat.CoordinateSystem = MarsMJ2000Eq;
GMAT Sat.DisplayStateType = Cartesian;
GMAT Sat.X = -40591519.6020276;
GMAT Sat.Y = 58617083.18761726;
GMAT Sat.Z = 32338710.37350587;
GMAT Sat.VX = -8.130502664289201;
GMAT Sat.VY = 12.18519028656942;
GMAT Sat.VZ = 2.815360357654711;
GMAT Sat.DryMass = 850;
GMAT Sat.Cd = 2.2;
GMAT Sat.Cr = 1.8;
GMAT Sat.DragArea = 15;
GMAT Sat.SRPArea = 1;
GMAT Sat.NAIFId = -10000001;
GMAT Sat.NAIFIdReferenceFrame = -9000001;
GMAT Sat.OrbitColor = Red;
GMAT Sat.TargetColor = Teal;
GMAT Sat.OrbitErrorCovariance = [ 1e+070 0 0 0 0 0 ; 0 1e+070 0 0 0 0 ; 0 0 1e+070 0 0 0 ; 0 0 0 1e+070 0 0 ; 0 0 0 0 1e+070 0 ; 0 0 0 0 0 1e+070 ];
GMAT Sat.CdSigma = 1e+070;
GMAT Sat.CrSigma = 1e+070;
GMAT Sat.Id = 'SatId';
GMAT Sat.Attitude = CoordinateSystemFixed;
GMAT Sat.SPADSRPScaleFactor = 1;
GMAT Sat.ModelFile = 'aura.3ds';
GMAT Sat.ModelOffsetX = 0;
GMAT Sat.ModelOffsetY = 0;
GMAT Sat.ModelOffsetZ = 0;
GMAT Sat.ModelRotationX = 0;
GMAT Sat.ModelRotationY = 0;
GMAT Sat.ModelRotationZ = 0;
GMAT Sat.ModelScale = 1;
GMAT Sat.AttitudeDisplayStateType = 'Quaternion';
GMAT Sat.AttitudeRateDisplayStateType = 'AngularVelocity';
GMAT Sat.AttitudeCoordinateSystem = EarthMJ2000Eq;
GMAT Sat.EulerAngleSequence = '321';

%----------------------------------------
%---------- ForceModels
%----------------------------------------

Create ForceModel DefaultProp_ForceModel;
GMAT DefaultProp_ForceModel.CentralBody = Mars;
GMAT DefaultProp_ForceModel.PrimaryBodies = {Mars};
GMAT DefaultProp_ForceModel.Drag = None;
GMAT DefaultProp_ForceModel.SRP = Off;
GMAT DefaultProp_ForceModel.RelativisticCorrection = Off;
GMAT DefaultProp_ForceModel.ErrorControl = RSSStep;
GMAT DefaultProp_ForceModel.GravityField.Mars.Degree = 4;
GMAT DefaultProp_ForceModel.GravityField.Mars.Order = 4;
GMAT DefaultProp_ForceModel.GravityField.Mars.StmLimit = 100;
GMAT DefaultProp_ForceModel.GravityField.Mars.PotentialFile = 'Mars50c.cof';
GMAT DefaultProp_ForceModel.GravityField.Mars.TideModel = 'None';

%----------------------------------------
%---------- Propagators
%----------------------------------------

Create Propagator DefaultProp;
GMAT DefaultProp.FM = DefaultProp_ForceModel;
GMAT DefaultProp.Type = RungeKutta89;
GMAT DefaultProp.InitialStepSize = 60;
GMAT DefaultProp.Accuracy = 9.999999999999999e-012;
GMAT DefaultProp.MinStep = 0.001;
GMAT DefaultProp.MaxStep = 2700;
GMAT DefaultProp.MaxStepAttempts = 50;
GMAT DefaultProp.StopIfAccuracyIsViolated = true;

%----------------------------------------
%---------- Coordinate Systems
%----------------------------------------

Create CoordinateSystem MarsMJ2000Eq;
GMAT MarsMJ2000Eq.Origin = Mars;
GMAT MarsMJ2000Eq.Axes = MJ2000Eq;

%----------------------------------------
%---------- Subscribers
%----------------------------------------

Create OrbitView DefaultOrbitView;
GMAT DefaultOrbitView.SolverIterations = Current;
GMAT DefaultOrbitView.UpperLeft = [ 0.001592356687898089 0 ];
GMAT DefaultOrbitView.Size = [ 0.5 0.45 ];
GMAT DefaultOrbitView.RelativeZOrder = 26;
GMAT DefaultOrbitView.Maximized = false;
GMAT DefaultOrbitView.Add = {Sat, Mars};
GMAT DefaultOrbitView.CoordinateSystem = MarsMJ2000Eq;
GMAT DefaultOrbitView.DrawObject = [ true true ];
GMAT DefaultOrbitView.DataCollectFrequency = 1;
GMAT DefaultOrbitView.UpdatePlotFrequency = 50;
GMAT DefaultOrbitView.NumPointsToRedraw = 0;
GMAT DefaultOrbitView.ShowPlot = true;
GMAT DefaultOrbitView.MaxPlotPoints = 20000;
GMAT DefaultOrbitView.ShowLabels = true;
GMAT DefaultOrbitView.ViewPointReference = Mars;
GMAT DefaultOrbitView.ViewPointVector = [ 30000 0 0 ];
GMAT DefaultOrbitView.ViewDirection = Mars;
GMAT DefaultOrbitView.ViewScaleFactor = 1;
GMAT DefaultOrbitView.ViewUpCoordinateSystem = MarsMJ2000Eq;
GMAT DefaultOrbitView.ViewUpAxis = Z;
GMAT DefaultOrbitView.EclipticPlane = Off;
GMAT DefaultOrbitView.XYPlane = On;
GMAT DefaultOrbitView.WireFrame = Off;
GMAT DefaultOrbitView.Axes = On;
GMAT DefaultOrbitView.Grid = Off;
GMAT DefaultOrbitView.SunLine = Off;
GMAT DefaultOrbitView.UseInitialView = On;
GMAT DefaultOrbitView.StarCount = 7000;
GMAT DefaultOrbitView.EnableStars = On;
GMAT DefaultOrbitView.EnableConstellations = On;

Create GroundTrackPlot DefaultGroundTrackPlot;
GMAT DefaultGroundTrackPlot.SolverIterations = Current;
GMAT DefaultGroundTrackPlot.UpperLeft = [ 0.001592356687898089 0.4526315789473684 ];
GMAT DefaultGroundTrackPlot.Size = [ 0.5 0.45 ];
GMAT DefaultGroundTrackPlot.RelativeZOrder = 20;
GMAT DefaultGroundTrackPlot.Maximized = false;
GMAT DefaultGroundTrackPlot.Add = {Sat, Earth};
GMAT DefaultGroundTrackPlot.DataCollectFrequency = 1;
GMAT DefaultGroundTrackPlot.UpdatePlotFrequency = 50;
GMAT DefaultGroundTrackPlot.NumPointsToRedraw = 0;
GMAT DefaultGroundTrackPlot.ShowPlot = true;
GMAT DefaultGroundTrackPlot.MaxPlotPoints = 20000;
GMAT DefaultGroundTrackPlot.CentralBody = Mars;
GMAT DefaultGroundTrackPlot.TextureMap = 'Mars_JPLCaltechUSGS.jpg';


%----------------------------------------
%---------- Mission Sequence
%----------------------------------------

BeginMissionSequence;
Propagate DefaultProp(Sat) {Sat.ElapsedSecs = 12000.0};
