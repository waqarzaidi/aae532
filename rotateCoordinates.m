function out = rotateCoordinates(x,inputFlag,outputFlag)
% Input: Cartesian or Keplerian State Vector 
%        inputFlag = 1 for Keplerian, 2 for Cartesian
%        outputFlag = 1 for Keplerian, 2 for Cartesian
% Cartesian State Vector = [x, y, z, vx, vy, vz] in km and km/s
% Keplerian State Vector = [a, e, i, Omega, omega, nu] in km and rad
% Output: Cartesian or Keplerian State Vector depending on input 

constants

if inputFlag == 1 && outputFlag == 2
    a = x(1); e = x(2); i = x(3); 
    Omega = x(4); omega = x(5); nu = x(6);
    [r, v] = orb2eci(gEarth,[a;e;i;Omega;omega;nu]);
    out = [r;v];
elseif inputFlag == 2 && outputFlag == 1
    r = [x(1);x(2);x(3)];
    v = [x(4);x(5);x(6)];
    coe = eci2orb1 (gEarth, r, v);
    out = coe;
end

