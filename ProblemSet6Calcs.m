clear all; close all; clc; 

% Problem Set 6 Calculations 
% Constants
global Re mu_e j2
Re      = 6378.137;     % km
Rm      = 3396.19;      % km
mu_e    = 398600.4418;  % km^3/s^2 
mu_m    = 42828.37190;  % km^3/s^2 
j2      = 0; %0.0010826269;
deg2rad = pi/180; 
rad2deg = 180/pi;

%% Problem 2
% Given
e     = 0.75;
a     = 4.4*Re;
TA    = 30;
ea2   = 180*deg2rad; 

%% part (a)
% part (i)
ea1   = 2*atan(((1+e)/(1-e))^(-0.5)*tand(TA/2));
TOF   = (sqrt(a^3/mu_e)*((ea2-e*sin(ea2))-(ea1-e*sin(ea1))))/3600;

% part (ii)

ra    = a*(1+e);
vp    = sqrt(2*mu_e/ra-mu_e/a);
vc    = sqrt(mu_e/ra);
dv    = vc-vp;

% part (iii)
p     = a*(1-e^2);
p1    = ra;
e1    = 0;
TA    = linspace(0,360,100);

for i = 1:size(TA,2)
    r(i) =  p/(1+e*cos(TA(1,i)*pi/180));
    r1(i) = p1/(1+e1*cos(TA(1,i)*pi/180));
end

for i = 1:size(TA,2)
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
    e_r1(i) = r1(i)*cos(TA(i)*pi/180);
    theta_r1(i) = r1(i)*sin(TA(i)*pi/180);    
end
figure(1)
plot(e_r,theta_r,'r-','LineWidth',2); grid on; hold on;
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(0,0,'bo',0,0,'bx');
axis equal
xlabel('ehat, km'); ylabel('phat, km');

%% part (b)
% part (i)
r2    = 6.6*Re;
TA    = acosd(1/e*(p/r2-1));

% part (ii)
ea2   = 2*atan(((1+e)/(1-e))^(-0.5)*tand(-TA/2));
% ea2   = ea2 - 2*pi;
TOF   = (sqrt(a^3/mu_e)*((ea2-e*sin(ea2))-(ea1-e*sin(ea1))))/3600 + 2*pi*sqrt(a^3/mu_e)/3600; 
v2    = sqrt(2*(mu_e/r2-mu_e/2/a));
g     = acosd(sqrt(a*(1-e^2)*mu_e)/r2/v2);

if ea2 < 0 && g > 0
    g = 360-g;
end

% part (iii)
vhat  = [sind(g);cosd(g)];
v2c   = sqrt(mu_e/r2);
dv    = sqrt(v2^2 + v2c^2 - 2*v2*v2c*cosd(g));
beta  = asind(v2c/dv*sind(-g));
alpha = 180 - beta;

% part (iv)
p1    = r2;
e1    = 0;
TA    = linspace(0,360,100);

for i = 1:size(TA,2)
    r1(i) = p1/(1+e1*cos(TA(1,i)*pi/180));
end

for i = 1:size(TA,2)
    e_r1(i) = r1(i)*cos(TA(i)*pi/180);
    theta_r1(i) = r1(i)*sin(TA(i)*pi/180);    
end
figure(2)
plot(e_r,theta_r,'r-','LineWidth',2); grid on; hold on;
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(0,0,'bo',0,0,'bx');
axis equal
xlabel('ehat, km'); ylabel('phat, km');

%% Problem 2
% Given 
e     = 0.4; 
a     = 3*Re;
TA    = 100;
TA1   = TA;
dv    = 0.9;
alpha = 112;

% part (a)
% part (b)
p     = a*(1-e^2);
r     = p/(1+e*cosd(TA));
v     = sqrt(2*mu_e/r-mu_e/a);
rdTA  = sqrt(mu_e*p)/r;
vhat  = [-sqrt(v^2-(sqrt(mu_e*p)/r)^2);sqrt(mu_e*p)/r];
dr    = sqrt(v^2 - rdTA^2);
g     = acosd(sqrt(a*(1-e^2)*mu_e)/r/v);
phi   = alpha + g;
dvhat = dv*[sind(phi);cosd(phi)]; % this is LVLH or r, theta, h
dvhp  = dv*[sind(phi)*cosd(TA)+cosd(phi)*(-sind(TA));sind(phi)*sind(TA)+cosd(phi)*cosd(TA)];
dvhat1= dv*[cosd(alpha);sind(alpha)]; % this VNC or VNB

% part (c) 
Beta  = 180-alpha;
v2    = sqrt(dv^2+v^2-2*dv*v*cosd(Beta));
dg    = asind(dv/v2*sind(Beta));
g2    = dg+g;
vhat  = v2*[sind(g2);cosd(g2)];

% part (d) 
h     = v2*r*cosd(g2);
p     = h^2/mu_e; 
a     = -mu_e/(v2^2-2*mu_e/r);
e     = sqrt(1-p/a);
rp    = a*(1-e);
TA    = acosd(1/e*(p/r-1));
Ener     = -mu_e/2/a;
P     = 2*pi*sqrt(a^3/mu_e)/3600;
ea2   = 2*atan(((1+e)/(1-e))^(-0.5)*tand(TA/2));
ea1   = 0;
tp    = (sqrt(a^3/mu_e)*((ea2-e*sin(ea2))-(ea1-e*sin(ea1))))/3600
dw    = TA1-TA;

%% Problem 3
% Given 
a     = 5*Rm;
e     = 0.5;
incl  = 30;
RA    = 45;
w     = -60;
M     = 90;
n     = sqrt(mu_m/a^3);
tp    = M*deg2rad/n;
t2    = tp + 500*60;
M2    = t2*n*rad2deg;
[eanom, tanom] = kepler1(M*deg2rad, e);
[eanom2, tanom2] = kepler1(M2*deg2rad, e);
    
eanom  = eanom*rad2deg;
tanom  = tanom*rad2deg;
eanom2 = eanom2*rad2deg;
tanom2 = tanom2*rad2deg;

p     = a*(1-e^2);
h     = sqrt(mu_m*p);
coe2  = [h e RA*deg2rad incl*deg2rad w*deg2rad (tanom2)*deg2rad];

[rhatInert2, vhatInert2] = sv_from_coe(coe2,mu_m)

r_m   = [0.1;-0.2;0.25];

g     = acosd(sqrt(mu_m*p)/norm(rhatInert2)/norm(vhatInert2));

RcV   = [sind(-g) 0 cosd(-g); cosd(-g) 0 -sind(-g); 0 1 0];

r_m_o = RcV*r_m;

[xrdl, yrdl, zrdl] = eci2rdl (rhatInert2, vhatInert2);

DCM = [xrdl;yrdl;zrdl];

dvr = DCM*r_m;

beta  = asind(dvr(3)/norm(dvr));

phi   = acosd(dvr(2)/norm(dvr)/cosd(beta));

alpha = phi - (-g);

dvr(3)/norm(dvr)*100

v2    = sqrt(norm(dvr)^2+norm(vhatInert2)^2-2*norm(dvr)*norm(vhatInert2)*cosd(beta));

dg    = asind(norm(dvr)/v2*sind(beta));
g2    = dg+g;

v2hat = vhatInert2' + r_m;

coe = coe_from_sv(rhatInert2,v2hat',mu_m);

TA    = coe(6);
e     = coe(2);
gnew  = atand(e*sin(TA)/(1+e*cos(TA)));

ea2   = 2*atan(((1+e)/(1-e))^(-0.5)*tan(TA/2));
M     = ea2-sin(ea2);

