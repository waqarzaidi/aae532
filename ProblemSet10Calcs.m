clear all; close all; clc; format longg;

% Problem Set 10 Calculations 
constants

%% Problem 1 (from PS10 Fall 2016 Problem 2)
% Given
r1     = 2.5*rEarth;
r2     = 6*rEarth;
TA     = 150; %270;
% r1     = 1.5*rEarth;
% r2     = 4*rEarth;
% TA     = 120;
% r1     = 1.25*rEarth;
% r2     = 3*rEarth;
% TA     = 135;

% part (a) 
aT     = 0.5*(r1+r2);
nT     = 1/sqrt(aT^3/gEarth);
v1     = sqrt(2*gEarth/r1-gEarth/aT);
v2     = sqrt(2*gEarth/r2-gEarth/aT);
v1Neg  = sqrt(gEarth/r1);
dv1    = v1-v1Neg;
v2Plus = sqrt(gEarth/r2);
dv2    = v2Plus-v2;
total  = dv2+dv1;
TOF    = pi*sqrt(aT^3/gEarth);
n_otv  = sqrt(gEarth/r1^3);
n_ss   = sqrt(gEarth/r2^3);
phi    = (pi - TOF*(n_ss))*rad2deg;
sOld   = 2*pi/(n_otv-n_ss);

% part (b) 
c      = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s      = 1/2*(r1+r2+c);
am     = 1/2*s;
alpham = 2*asin(sqrt(s/2/am));
betam  = 2*asin(sqrt((s-c)/2/am));
TOFm   = sqrt(am^3/gEarth)*(alpham--betam-sin(alpham)+sin(-betam));
pm     = (4*am*(s-r1)*(s-r2))/c^2*(sin((alpham+betam)/2)^2);
em     = sqrt(1-pm/am);
rpm    = am*(1-em);
% thetaD = -acosd((pm-r1)/(r1*em));
% thetaA = acosd((pm-r2)/(r2*em));
thetaD = acosd((pm-r1)/(r1*em));
thetaA = 360-acosd((pm-r2)/(r2*em));
thetaA-thetaD
vD     = sqrt(gEarth*(2/r1-1/am));
vA     = sqrt(gEarth*(2/r2-1/am));
h      = sqrt(gEarth*pm);
% fpaD   = -acosd(h/r1/vD);
% fpaA   = acosd(h/r2/vA);
fpaD   = acosd(h/r1/vD);
fpaA   = -acosd(h/r2/vA);
% E_D    = -acos(1/em*(1-r1/am));
% E_A    = acos(1/em*(1-r2/am));
E_D    = acos(1/em*(1-r1/am));
E_A    = -acos(1/em*(1-r2/am));
tdtp   = (E_D-em*sin(E_D))*sqrt(am^3/gEarth);
tatp   = (E_A-em*sin(E_A))*sqrt(am^3/gEarth);
TOFm   = tatp-tdtp;
phim   = (TA*deg2rad - TOFm*sqrt(gEarth/r2^3))*rad2deg;

% part (c) 
dv1    = sqrt(v1Neg^2 + vD^2 - 2*v1Neg*vD*cosd(fpaD));
% alphaD = -(180+asind(vD*sind(fpaD)/dv1))
alphaD = asind(vD*sind(fpaD)/dv1)
dv1vec = dv1*[cosd(alphaD);0;sind(alphaD)]

dv2    = sqrt(v2Plus^2 + vA^2 - 2*v2Plus*vA*cosd(fpaA));
% alphaA = -asind(v2Plus/dv2*sind(fpaA))
alphaA = -asind(v2Plus/dv2*sind(fpaA))
dv2vec = dv2*[cosd(alphaA);0;sind(alphaA)]

%% Problem 2
% Given 
rPark  = 250 + rEarth; 
rArr   = 1000 + rMars;

% part (a) (i)
aA     = aEarth;
aB     = aMars;
v1  = sqrt(gSun/aA);
v2   = sqrt(gSun/aB);
aT     = (aEarth + aMars)/2;

% Orbit 1
h1     = sqrt(2*gSun)*sqrt(aA*aA/(aA+aA));
vA1    = h1/aA;

% Orbit 2 
h2     = sqrt(2*gSun)*sqrt(aA*aB/(aA+aB));
vA2    = h2/aA;
vB2    = h2/aB;

% Orbit 3 
h3     = sqrt(2*gSun)*sqrt(aB*aB/(aB+aB));
vB3    = h3/aB;

% Total Delta-V 
dvA    = vA2 - vA1; 
dvB    = vB3 - vB2;

tdv    = dvA + dvB;

TOFhom = pi*sqrt(aT^3/gSun);
nEarth = sqrt(gSun/aEarth^3);
nMars  = sqrt(gSun/aMars^3);
phi    = (pi - TOFhom*(nMars))*rad2deg;
sHom   = 2*pi/(nEarth-nMars);

% alternative approach (a) 
v1c    = sqrt(gSun/aEarth)
v2c    = sqrt(gSun/aMars)
vPt    = sqrt(gSun*(2/aEarth-1/aT))
vAt    = sqrt(gSun*(2/aMars-1/aT))
dv1    = vPt - v1c
dv2    = v2c - vAt
total  = abs(dv1) + abs(dv2)

% part (b)
TA     = 160;
r1     = aA; 
r2     = aB;
c      = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s      = 1/2*(r1+r2+c);
am     = 1/2*s;
Enerm  = -gSun/2/am;
alpham = 2*asin(sqrt(s/2/am));
betam  = 2*asin(sqrt((s-c)/2/am));
% TOFm   = sqrt(am^3/gEarth)*(alpham-betam-sin(alpham)+sin(betam));
pm     = (4*am*(s-r1)*(s-r2))/c^2*(sin((alpham+betam)/2)^2);
em     = sqrt(1-pm/am);
rpm    = am*(1-em);
rpa    = am*(1+em);
thetaD = acosd((pm-r1)/(r1*em));
thetaA = 360-acosd((pm-r2)/(r2*em));
thetaA-thetaD
vD     = sqrt(gSun*(2/r1-1/am));
vA     = sqrt(gSun*(2/r2-1/am));
h      = sqrt(gSun*pm);
fpaD   = acosd(h/r1/vD);
fpaA   = -acosd(h/r2/vA);
E_D    = acos(1/em*(1-r1/am));
E_A    = -acos(1/em*(1-r2/am));
tdtp   = (E_D-em*sin(E_D))*sqrt(am^3/gSun);
tatp   = (E_A-em*sin(E_A))*sqrt(am^3/gSun);
TOFm   = tdtp-tatp;

% plots 
% p1    = aEarth*(1-eEarth^2); 
% p2    = aMars*(1-eMars^2); 
% p3    = pm; 
% e1    = eEarth; 
% e2    = eMars; 
% e3    = em; 
% TA    = linspace(0,360,100);
% 
% for i = 1:size(TA,2)
%     r1(i) = p1/(1+e1*cosd(TA(1,i)));
%     r2(i) = p2/(1+e2*cosd(TA(1,i)));
%     r3(i) = p3/(1+e3*cosd(TA(1,i)));    
% %     r4(i) = pIapetus/(1+eIapetus*cosd(TA(1,i)));  
% %     r5(i) = pHyperion/(1+eHyperion*cosd(TA(1,i))); 
% end
% 
% for i = 1:size(TA,2)
%     e_r1(i) = r1(i)*cosd(TA(i));
%     theta_r1(i) = r1(i)*sind(TA(i));    
%     e_r2(i) = r2(i)*cosd(TA(i));
%     theta_r2(i) = r2(i)*sind(TA(i));
%     e_r3(i) = r3(i)*cosd(TA(i));
%     theta_r3(i) = r3(i)*sind(TA(i));    
% %     e_r4(i) = r4(i)*cosd(TA(i));
% %     theta_r4(i) = r4(i)*sind(TA(i));  
% %     e_r5(i) = r5(i)*cosd(TA(i));
% %     theta_r5(i) = r5(i)*sind(TA(i));      
% end
% figure(2)
% plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
% plot(e_r2,theta_r2,'r-','LineWidth',2); hold on;
% plot(e_r3,theta_r3,'g-','LineWidth',2); hold on;
% % plot(e_r4,theta_r4,'g-','LineWidth',2); hold on;
% % plot(e_r5,theta_r5,'m-','LineWidth',2); hold on;
% % plot3(0,0,0,'bo',0,0,0,'ko','MarkerSize',7);
% [x,y,z] = sphere(20);
% surf(250*rSaturn*x,250*rSaturn*y,250*rSaturn*z); hold on;
% axis equal
% xlabel('ehat, km'); ylabel('phat, km'); grid on;
% legend('Earth','Mars','Trajectory')
% 
% %% David's code
% figure()
% t = linspace (0,2*pi);
% t3 = linspace (0.60,3.391);
% t4 = linspace (0.60,-2.892);
% Earth_SM = aEarth; % 1.496e8;
% Mars_SM = aMars; %2.2795e8;
% a_min = am; %1.874e8;
% e_min = em; %0.226;
% ae = a_min*e_min;
% b_min = a_min*((1-(e_min^2))^0.5);
% rt1= (-34.4/57.3);
% y_correction = sin(abs(rt1))*(ae);
% osc = (((ae^2)+(ae^2))-(2*ae*ae*cos(rt1)))^0.5;
% n = acos((y_correction)/(osc));
% x_correction = sin(n)*osc;
% x_earth = 0;
% y_earth = 0;
% x = a_min*cos(t3)*cos(rt1) - b_min*sin(t3)*sin(rt1)-(a_min*e_min)+ x_correction;
% y = a_min*cos(t3)*sin(rt1) + b_min*sin(t3)*cos(rt1)+ y_correction;
% x7 = a_min*cos(t4)*cos(rt1) - b_min*sin(t4)*sin(rt1)-(a_min*e_min)+ x_correction;
% y7 = a_min*cos(t4)*sin(rt1) + b_min*sin(t4)*cos(rt1)+ y_correction;
% x5 = aEarth*cos(t); % Earth_SM*cos(t);
% y5 = aEarth*sin(t); % Earth_SM*sin(t);
% x6 = aMars*cos(t); % Mars_SM*cos(t);
% y6 = aMars*sin(t); % Mars_SM*sin(t);
% t2 = linspace (-1.5,1.5);
% rt = (90/57.3);
% x2 = (1*cosh(t2)*cos(rt))-(1*sinh(t2)*sin(rt));
% x21 = (-1*cosh(t2)*cos(rt))+(1*sinh(t2)*sin(rt));
% y2 = (1*cosh(t2)*sin(rt))+(1*sinh(t2)*cos(rt));
% y21 = (-1*cosh(t2)*sin(rt))+(-1*sinh(t2)*cos(rt));
% plot (x,y,'g-','LineWidth',2)
% hold on
% plot (x5,y5,'b-','LineWidth',2)
% hold on
% plot (x6,y6,'r-','LineWidth',2)
% hold on
% plot (x7,y7,'k-','LineWidth',2)
% hold on
% % plot (x_earth,y_earth,'r*')
% [x,y,z] = sphere(20);
% surf(250*rSaturn*x,250*rSaturn*y,250*rSaturn*z); hold on;
% axis equal
% xlabel('ehat, km'); ylabel('phat, km'); grid on;
% legend('Transfer Arc','Earth','Mars','Transfer Orbit')
% grid minor 

%%

% part (c)
phim   = (TA*deg2rad - TOFm*sqrt(gSun/r2^3))*rad2deg;

% part (d)
dv1    = sqrt(v1^2 + vD^2 - 2*v1*vD*cosd(fpaD));
dv2    = sqrt(v2^2 + vA^2 - 2*v2*vA*cosd(fpaA));
vEarth = sqrt(gSun/aEarth);
fpaE   = -acosd(sqrt((aEarth*(1-eEarth^2)*gSun))/aEarth/vEarth);

vInfE  = sqrt(vEarth^2 + vD^2 - 2*vEarth*vD*cosd(abs(fpaD)));

vMars  = sqrt(gSun/aMars);
fpaM   = -acosd(sqrt((aMars*(1-eMars^2)*gSun))/aMars/vMars);

vInfM  = sqrt(vMars^2 + vA^2 - 2*vMars*vA*cosd(abs(fpaA)));

v_Dv   = sqrt(vInfE^2 + 2*gEarth/rPark) - sqrt(gEarth/rPark);

v_Av   = sqrt(vInfM^2 + 2*gMars/rArr) - sqrt(gMars/rArr);

%% Problem 3 
% part (a) 
fun = @root1d;
x0 = 5*rEarth;
x = fsolve(fun,x0)
am = x;
r1     = 2.5*rEarth;
r2     = 6*rEarth;
TA     = 270;
c      = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s      = 1/2*(r1+r2+c);
alpham = 2*asin(sqrt(s/2/am));
betam  = 2*asin(sqrt((s-c)/2/am));
pm     = (4*am*(s-r1)*(s-r2))/c^2*(sin((alpham+betam)/2)^2);
em     = sqrt(1-pm/am);
rpm    = am*(1-em);
thetaD = -acosd((pm-r1)/(r1*em));
thetaA = 360-acosd((pm-r2)/(r2*em));
thetaA-thetaD
vD     = sqrt(gEarth*(2/r1-1/am));
vA     = sqrt(gEarth*(2/r2-1/am));
h      = sqrt(gEarth*pm);
fpaD   = -acosd(h/r1/vD);
fpaA   = acosd(h/r2/vA);
dv1    = sqrt(v1Neg^2 + vD^2 - 2*v1Neg*vD*cosd(fpaD));
alphaD = asind(vD*sind(fpaD)/dv1)
dv1vec = dv1*[cosd(alphaD);0;sind(alphaD)]
dv2    = sqrt(v2Plus^2 + vA^2 - 2*v2Plus*vA*cosd(fpaA));
alphaA = 180-asind(v2Plus/dv2*sind(fpaA))
dv2vec = dv2*[cosd(alphaA);0;sind(alphaA)]

function F = root1d(x)
rEarth = 6378.137;
gEarth = 398600.4418;
r1     = 2.5*rEarth;
r2     = 6*rEarth;
TA     = 270;
c      = sqrt(r1^2 + r2^2 - 2*r1*r2*cosd(TA));
s      = 1/2*(r1+r2+c);
alpham = 2*asin(sqrt(s/2/x));
betam  = 2*asin(sqrt((s-c)/2/x));
alphan = 2*pi - alpham;
betan  = -betam;
F = sqrt(x^3/gEarth)*(alphan-betan-sin(alphan)+sin(betan)) - 14*3600;
end