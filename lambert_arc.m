function [a_new, e_new, p_new, type_n, type_l,TA,alpha_f,beta_f] = ...
    lambert_arc(r1_vec,r2_vec,TOF_given, mu_sun)



r1 = norm(r1_vec);
r2 = norm(r2_vec);

TA = acosd(dot(r1_vec,r2_vec)/(r1*r2));
%[deg] Transfer angle for the space triangle.

if TA < 180
    type_n = 1;
elseif TA >180
    type_n = 2;
end

c = norm(r2_vec - r1_vec);
% Chord of the of the space triangle [km]
s = (r1 + r2 + c)/2;

a_min = s/2;

alpha_min = 2 * asind( sqrt(s/(2*a_min)) ); 
% [deg] space triangle parameter alpha
beta_min = 2 * asind( sqrt( (s-c)/(2*a_min) ) );
% [deg] space triangle parameter beta

p_min1 = ( ( 4 * a_min * (s-r1) * (s-r2) )/(c^2) ) * sind((alpha_min - beta_min)/2)^2;
p_min2 = ( ( 4 * a_min * (s-r1) * (s-r2) )/(c^2) ) * sind((alpha_min + beta_min)/2)^2;
% [km] semi-latus parameter

e_min = sqrt(1 - p_min1/a_min);

h_min = sqrt(p_min1 * mu_sun);


%% TOF Check the type of Conic


if type_n == 1
    
    TOF_par = (1/3) * sqrt(2/mu_sun) * (s^(3/2) - (s-c)^(3/2));
    % TOF_par_hours = TOF_par/(3600);
elseif type_n == 2
    
    TOF_par = (1/3) * sqrt(2/mu_sun) * (s^(3/2) + (s-c)^(3/2));
    % TOF_par_hours = TOF_par/(3600);
    
end



if TOF_given > TOF_par
    conic = 'ellipse';
    
elseif TOF_given < TOF_par
    conic = 'hyperbola';
else
    conic = 'parabola';
end



if strcmp(conic,'ellipse')
    
    alpha_min_rad = deg2rad(alpha_min);
    beta_min_rad = deg2rad(beta_min);
    
    TOF_min = sqrt(a_min^3 / mu_sun) * ( (alpha_min_rad - sin(alpha_min_rad)) - (beta_min_rad - sin(beta_min_rad)) );
    % TOF_min2 = sqrt(a_min^3 / mu_earth) * ( (alpha_min_rad - beta_min_rad ) - (sin(alpha_min_rad) -sin(beta_min_rad)) );
    % TOF_min_hours = TOF_min/3600;
    % TOF_min_2_hours = TOF_min2/3600;

    if TOF_given < TOF_min
        type_l = 'A';
        
    elseif TOF_given > TOF_min
        type_l = 'B';
        
    end
    
end

    

%% Iterate and find lambert Parameters

if strcmp(conic,'hyperbola')
    
    a_init = a_min*20;
    
    [a_abs_new,TOF_res,alpha_f,beta_f, fval] = a_iter_hyp(a_init,s,c,TOF_given,mu_sun,type_n);
%     [a_abs_new] = a_iter2_hyp(a_init, s,c,TOF_given,mu_sun,type_n);
    
    p_hyp1 = ((4*a_abs_new*(s-r1)*(s-r2))/(c^2) ) * (sinh((alpha_f + beta_f)/2)^2);
    p_hyp2 = ((4*a_abs_new*(s-r1)*(s-r2))/(c^2) ) * (sinh((alpha_f - beta_f)/2)^2);
    
    p_hyp_vec = [p_hyp1,p_hyp2];
    
    if type_n == 1
        p_hyp = max(p_hyp_vec);
    elseif type_n ==2
        p_hyp = min(p_hyp_vec);
    end
    e_hyp = sqrt(1 + p_hyp/a_abs_new);
    
    a_new = a_abs_new;
    e_new = e_hyp;
    p_new = p_hyp

    
elseif strcmp(conic,'ellipse')
    
    a_init = a_min;
%     [a_res,TOF_res,alpha_f, beta_f, fval] = a_iter_ellipse(a_init, s,c,TOF,mu,type_n,type_l)
    [a_new,TOF_res ,alpha_f ,beta_f, fval] = a_iter_ellipse(a_init,s,c,TOF_given,mu_sun,type_n,type_l);
    
    p_1 = ((4*a_new*(s-r1)*(s-r2))/(c^2) ) * (sin((alpha_f + beta_f)/2)^2);
    p_2 = ((4*a_new*(s-r1)*(s-r2))/(c^2) ) * (sin((alpha_f - beta_f)/2)^2);
    
    p_vec = [p_1,p_2];
    
    if (type_n == 1 && type_l == 'A') || (type_n == 2 && type_l == 'B')
        p = max(p_vec);
    elseif (type_n == 1 && type_l == 'B') || (type_n == 2 && type_l == 'A')
        p = min(p_vec);
    end
    e_new = sqrt(1 - p/a_new);
    
    p_new = p;
    
end


