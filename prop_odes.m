function dfdt = prop_odes(~,f)
constants
j2 = 0;
Re = 0;
%...For ease of Reading the code, assign each component of f
%...to a mnemonic variable:
r1x = f( 1);
r1y = f( 2);
r1z = f( 3);
v1x = f( 4);
v1y = f( 5);
v1z = f( 6);
r = norm([r1x, r1y, r1z]);
a1x = -gSun*r1x/r^3*(1-3/2*j2*(Re/r)^2*(5*r1z^2/r^2-1));
a1y = r1y/r1x*a1x;
a1z = -gSun*r1z/r^3*(1-3/2*j2*(Re/r)^2*(5*r1z^2/r^2-3));
dfdt = [v1x;
v1y;
v1z;
a1x;
a1y;
a1z];