clear all; close all; clc; 

% Problem Set 5 Calculations 
% Constants
global Re mu_e j2
Re      = 6378.137;     % km
Rm      = 3396.19;      % km
mu_e    = 398600.4418;  % km^3/s^2 
mu_m    = 42828.37190;  % km^3/s^2 
j2      = 0; %0.0010826269;
deg2rad = pi/180; 
rad2deg = 180/pi;

%% Problem 2
% Given
rp    = 1.5*Re;
ra    = 200*Re;
w     = 45; 
TA    = 0;

%% Problem 2
% Given
rp    = 1.8*Rm;
ra    = 8*Rm;
M     = -120; 
frac  = 3/4;

% Given
% rp    = 1.65*Rm;
% ra    = 7*Rm;
% M     = 60; 
% frac  = 2/3;

% part (a)
a     = 0.5*(rp+ra);
e     = 1-rp/a;
p     = a*(1-e^2);
h     = sqrt(mu_m*p);
T     = 2*pi*sqrt(a^3/mu_m);
Ener  = -mu_m/2/a;

[eanom, tanom] = kepler1(M*deg2rad, e);

r     = a*(1-e*cos(eanom));
v     = sqrt(2*(Ener+mu_m/r));
n     = sqrt(mu_m/a^3); 
tp1   = mod(M,360)*deg2rad/n; 
g     = atan(e*sin(tanom)/(1+e*cos(tanom)));

if tp1 < 0
    tp1 = tp1 + 1/n;
end

% part (b)
rh    = [cos(tanom);sin(tanom)]*r;
v1    = [sin(g);cos(g)]*v;
v2    = v1(1)*[cos(tanom);sin(tanom)];
v3    = v1(2)*[-sin(tanom);cos(tanom)];
vh    = v2+v3;

fprintf(strcat('Problem 2a','\n'));
fprintf(strcat('---------','\n'));
fprintf(strcat('semi-major axis (km) =',32,num2str(a),'\n'));
fprintf(strcat('eccentricity =',32,num2str(e),'\n'));
fprintf(strcat('orbital angular momentum (km^2/s) =',32,num2str(h),'\n'));
fprintf(strcat('orbital period (hr) =',32,num2str(T/3600),'\n'));
fprintf(strcat('orbital energy (km^2/s^2) =',32,num2str(Ener),'\n'));
fprintf(strcat('orbital radius (km) =',32,num2str(r),'\n'));
fprintf(strcat('orbital velocity (km/s) =',32,num2str(v),'\n'));
fprintf(strcat('FPA (deg) =',32,num2str(g*rad2deg),'\n'));
fprintf(strcat('Eccentric Anonmaly (deg) =',32,num2str(mod(eanom*rad2deg,360)),'\n'));
fprintf(strcat('Time Since Periapsis (hrs) =',32,num2str(tp1/3600),'\n'));
fprintf(strcat('','\n'));
fprintf(strcat('Problem 2b','\n'));
fprintf(strcat('---------','\n'));
fprintf(strcat('Position in ehat and phat (km) =',32,num2str(rh'),'\n'));
fprintf(strcat('Velocity in ehat and phat (km/s) =',32,num2str(vh'),'\n'));

% part (c) 
tp2   = frac*T + tp1;

if tp2 < 0
    tp2 = tp2 + 1/n;
end

M2    = tp2*n;

[eanom2, tanom2] = kepler1(M2, e);

r2    = a*(1-e*cos(eanom2));
v2    = sqrt(2*(Ener+mu_m/r2));

% f and g functions in terms of true anomaly
f     = 1-r2/p*(1-cos(tanom2-tanom));
g     = r*r2/sqrt(mu_m*p)*sin(tanom2-tanom); 
fdot  = dot(rh,vh)/p/r*(1-cos(tanom2-tanom))-1/r*sqrt(mu_m/p)*sin(tanom2-tanom);
gdot  = 1-r/p*(1-cos(tanom2-tanom));
fprintf(strcat('','\n'));
fprintf(strcat('Problem 2c','\n'));
fprintf(strcat('---------','\n'));
fprintf(strcat('Eccentric Anonmaly (deg) =',32,num2str(eanom2*rad2deg),'\n'));
fprintf(strcat('True Anonmaly (deg) =',32,num2str(tanom2*rad2deg),'\n'));
fprintf(strcat('f and g functions using true anomaly','\n'));
fprintf(strcat('f =',32,num2str(f),'\n'));
fprintf(strcat('g =',32,num2str(g),'\n'));
fprintf(strcat('fdot =',32,num2str(fdot),'\n'));
fprintf(strcat('gdot =',32,num2str(gdot),'\n'));

test  = g + sqrt(a^3/mu_m)*(eanom2-eanom)-sin(eanom2-eanom);

% f and g functions in terms of eccentric anomaly 
f     = 1-a/r*(1-cos(eanom2-eanom));
g     = (tp2-tp1)-sqrt(a^3/mu_m)*((eanom2-eanom)-sin(eanom2-eanom)); 
fdot  = -sqrt(mu_m*a)/r2/r*sin(eanom2-eanom);
gdot  = 1-a/r2*(1-cos(eanom2-eanom));

fprintf(strcat('f and g functions using eccentric anomaly','\n'));
fprintf(strcat('f =',32,num2str(f),'\n'));
fprintf(strcat('g =',32,num2str(g),'\n'));
fprintf(strcat('fdot =',32,num2str(fdot),'\n'));
fprintf(strcat('gdot =',32,num2str(gdot),'\n'));

TOF  = (sqrt(a^3/mu_m)*((eanom2-e*sin(eanom2))-(eanom-e*sin(eanom))))/3600

TA = linspace(0,360,100);
Flight = linspace(0*rad2deg,tanom2*rad2deg,100);
Flight2 = linspace(tanom*rad2deg,360,100);

% Compute a vector with positions

n = size(TA,2);
m = size(Flight,2);

for i = 1:n
    r(i) =  p/(1+e*cos(TA(1,i)*pi/180));;
end

for i = 1:n
    v(i) = sqrt(2*(mu_m/2/a+mu_m/r(i)));
end

for i = 1:n
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
end

for i = 1:n
    e_v(i) = -v(i)*sin(TA(i)*pi/180);
    theta_v(i) = v(i)*cos(TA(i)*pi/180);
end

for i = 1:m
    r2(i) =  p/(1+e*cos(Flight(1,i)*pi/180));
    rf(i) =  p/(1+e*cos(Flight2(1,i)*pi/180));
end

for i = 1:m
    v2(i) = sqrt(2*(mu_m/2/a+mu_m/r2(i)));
    vf(i) = sqrt(2*(mu_m/2/a+mu_m/rf(i)));
end

for i = 1:m
    e_r2(i) = r2(i)*cos(Flight(i)*pi/180);
    theta_r2(i) = r2(i)*sin(Flight(i)*pi/180);
end

for i = 1:m
    e_v2(i) = -v2(i)*sin(Flight(i)*pi/180);
    theta_v2(i) = v2(i)*cos(Flight(i)*pi/180);
    e_rf(i) = rf(i)*cos(Flight2(i)*pi/180);
    theta_rf(i) = rf(i)*sin(Flight2(i)*pi/180);
end

figure(1)
plot(e_r,theta_r,'r-'); grid on; hold on;
plot(e_r2,theta_r2,'r-','LineWidth',2); hold on;
plot(e_rf,theta_rf,'r-','LineWidth',2); hold on;
hold on
plot(0,0,'bo',0,0,'bx');

%% Problem 3 
% Given
a     = 66*Re; 
e     = 0.9118; 
incl  = 34; 
RA    = 51;
w     = 0;
TA    = 235; % descending

% a     = 2.5*Re; 
% e     = 0.4; 
% incl  = 30; 
% RA    = 45; 
% w     = -90; 
% TA    = 235; % descending

% part (a) 
TA    = TA-w;
p     = a*(1-e^2);
h     = sqrt(mu_e*p);
r     = p/(1+e*cosd(TA));
Ener  = -mu_e/2/a;
v     = sqrt(2*(Ener+mu_e/r)); 
rhat  = [r;0;0];
rTAd  = h/r; 
rdot  = -sqrt(v^2 - (rTAd)^2);
vhat  = [rdot;rTAd;0];
g     = atan(e*sind(TA)/(1+e*cosd(TA)));
E     = 2*atan(((1+e)/(1-e))^(-0.5)*tand(TA/2));
M     = E-e*sin(E);
n     = sqrt(mu_e/a^3); 
tp    = M/n; 

if tp < 0
    tp = tp + 2*pi*1/n;
end

coe   = [h e RA*deg2rad incl*deg2rad w*deg2rad TA*deg2rad];

[rhatInert, vhatInert] = sv_from_coe(coe,mu_e);

% part (b) 
dt    = 7*3600; 
Mt    = M + n*dt;

% Calculate True and Eccentric Anomaly (Danby's method)
[eanom, tanom] = kepler1(Mt, e);

coe2   = [h e RA*deg2rad incl*deg2rad w*deg2rad (tanom+w)*deg2rad];

[rhatInert2, vhatInert2] = sv_from_coe(coe2,mu_e);


fprintf(strcat('Problem 3','\n'));
fprintf(strcat('---------','\n'));
fprintf(strcat('Position in Orbital Frame (km) =',32,num2str(rhat'),'\n'));
fprintf(strcat('Velocity in Orbital Frame (km/s) =',32,num2str(vhat'),'\n'));
fprintf(strcat('orbital radius (km) =',32,num2str(r),'\n'));
fprintf(strcat('orbital velocity (km/s) =',32,num2str(v),'\n'));
fprintf(strcat('FPA (deg) =',32,num2str(g*rad2deg),'\n'));
fprintf(strcat('ThetaStar (deg) =',32,num2str(TA),'\n'));
fprintf(strcat('Mean Anonmaly (deg) =',32,num2str(M*rad2deg),'\n'));
fprintf(strcat('Eccentric Anonmaly (deg) =',32,num2str(E*rad2deg),'\n'));
fprintf(strcat('Time Since Periapsis (hrs) =',32,num2str(tp/3600),'\n'));
fprintf(strcat('Position in Inertial Frame(km) =',32,num2str(rhatInert),'\n'));
fprintf(strcat('Velocity in Inertial Frame (km/s) =',32,num2str(vhatInert),'\n'));

% Calculate position and velocity in orbital frame 7 hours later
r     = p/(1+e*cos(tanom));
v     = sqrt(2*(Ener+mu_e/r)); 
g     = atan(e*sind(TA)/(1+e*cosd(TA)));
rhat  = [r;0;0];
rTAd  = h/r; 
rdot  = -sqrt(v^2 - (rTAd)^2);
vhat  = [rdot;rTAd;0];
% 
% rh    = [cos(tanom);sin(tanom)]*r;
% v1    = [sin(g);cos(g)]*v;
% v2    = v1(1)*[cos(tanom);sin(tanom)];
% v3    = v1(2)*[-sin(tanom);cos(tanom)];
% vh    = v2+v3;

fprintf(strcat('7 hours later...',32,'\n'))
fprintf(strcat('Eccentric Anonmaly (deg) =',32,num2str(eanom*rad2deg),'\n'));
fprintf(strcat('True Anomaly (deg) =',32,num2str(tanom*rad2deg),'\n'));
fprintf(strcat('Position in Orbital Frame (km) =',32,num2str(rhat'),'\n'));
fprintf(strcat('Velocity in Orbital Frame (km/s) =',32,num2str(vhat'),'\n'));

DCM = [cosd(RA)*cosd(TA)-sind(RA)*cosd(incl)*sind(TA)   -cosd(RA)*sind(TA)-sind(RA)*cosd(incl)*cosd(TA) sind(RA)*sind(incl);
       sind(RA)*cosd(TA)+cosd(RA)*cosd(incl)*sind(TA)   -sind(RA)*sind(TA)+cosd(RA)*cosd(incl)*cosd(TA) -cosd(RA)*sind(incl);
       sind(incl)*sind(TA)  sind(incl)*cosd(TA) cosd(incl)];

%% Problem 4
% part (a) 
% r1 = [3*Re;5*Re;0];
% v1 = [-3.2;2;2.5];

r1 = [0.15*Re;-1.44*Re;-0.65*Re];
v1 = [6.62;2.7;-1.56];


coe = coe_from_sv(r1,v1,mu_e);
r  = norm(r1);
v  = norm(v1);
g  = atan(coe(2)*sin(coe(6))/(1+coe(2)*cos(coe(6))));
E  = 2*atan(((1+coe(2))/(1-coe(2)))^(-0.5)*tan(coe(6)/2));
M  = E-coe(2)*sin(E);
n  = sqrt(mu_e/coe(7)^3);
tp = M/n; 
if tp < 0 
    tp = tp + 2*pi*1/n;
end

fprintf(strcat('','\n'));
fprintf(strcat('Problem 4','\n'));
fprintf(strcat('---------','\n'));
fprintf(strcat('Semi-Major Axis (km) =',32,num2str(coe(7)),'\n'));
fprintf(strcat('Eccentricity =',32,num2str(coe(2)),'\n'));
fprintf(strcat('Inclination (deg) =',32,num2str(coe(4)*rad2deg),'\n'));
fprintf(strcat('RAAN (deg) =',32,num2str(coe(3)*rad2deg),'\n'));
fprintf(strcat('Argument of Perigee (deg) =',32,num2str(coe(5)*rad2deg),'\n'));
fprintf(strcat('ThetaStar (deg) =',32,num2str(coe(6)*rad2deg),'\n'));
fprintf(strcat('Orbital Radius (km) =',32,num2str(r),'\n'));
fprintf(strcat('Orbital Velocity (km/s) =',32,num2str(v),'\n'));
fprintf(strcat('FPA (deg) =',32,num2str(g*rad2deg),'\n'));
fprintf(strcat('Mean Anonmaly (deg) =',32,num2str(M*rad2deg),'\n'));
fprintf(strcat('Eccentric Anonmaly (deg) =',32,num2str(E*rad2deg),'\n'));
fprintf(strcat('Time Since Periapsis (sec) =',32,num2str(tp),'\n'));

% ecc1store = []; inc1store = [];
% xOut = twobody([r1;v1],[0 1/n*10],60,'prop_odes');
% for i = 1:length(xOut)
%     ecc1store = [ecc1store; (cross(xOut(i,4:6),cross(xOut(i,1:3),xOut(i,4:6))))/mu_e - xOut(i,1:3)/norm(xOut(i,1:3))];
%     [p,a,ecc,incl,omega,argp,nu,m,arglat,truelon,lonper] = rv2coe (xOut(i,1:3),xOut(i,4:6),mu_e);
%     inc1store = [inc1store; incl*cos(omega), incl*sin(omega)];
% end
% 
% figure(); 
% subplot(2,2,1)
% plot(xOut(:,1),xOut(:,2),'b-'); grid on; hold on;
% xlabel('X Position, km'); ylabel('Y Position, km');
% subplot(2,2,2)
% plot(xOut(:,1),xOut(:,3),'b-'); grid on; hold on;
% xlabel('X Position, km'); ylabel('Z Position, km');
% subplot(2,2,3)
% plot(xOut(:,2),xOut(:,3),'b-'); grid on; hold on;
% xlabel('Y Position, km'); ylabel('Z Position, km');
% subplot(2,2,4)
% plot3(xOut(:,1),xOut(:,2),xOut(:,3),'b-'); grid on; hold on;
% xlabel('X Position, km'); ylabel('Y Position, km'); zlabel('Z Position, km');
% 
% figure();
% % plot(eccVec1(:,1),eccVec1(:,2),'b-'); grid on; hold on;
% plot(ecc1store(:,1),ecc1store(:,2),'r-');
% % plot(eccVec1(1,1),eccVec1(1,2),'bo'); hold on;
% % plot(eccVec1(end,1),eccVec1(end,2),'bx'); hold on;
% % plot(ecc1store(1,1),ecc1store(1,2),'ro'); hold on;
% % plot(ecc1store(end,1),ecc1store(end,2),'rx');
% xlabel('Eccentricity Vector X-Component','FontSize',14); ylabel ('Eccentricity Vector Y-Component','FontSize',14);
% % legend('SPG4','Two-Body')