function [a_res,TOF_res, alpha_f,beta_f, fval] = a_iter_hyp(a_init, s,c,TOF,mu,type_n)

%  TOF in seconds
% a_init in km
% mu in km^3/s^2


%% Type 2
if type_n == 2
% Only for type 2B
% TOF = TOF*3600;


% TOF_2B_f = TOF - sqrt(aa^3 / mu_earth) * ((alpha_f - sin(alpha_f)) - (beta_f - sin(beta_f)) );
TOF_2H_f = @(a) TOF - sqrt(a^3 / mu) * ((sinh(2 * asinh( sqrt(s/(2*a)) )) - 2 * asinh( sqrt(s/(2*a)) ) )...
    - (sinh(-2 * asinh( sqrt((s-c)/(2*a)) )) - (-2 * asinh( sqrt((s-c)/(2*a)) ))  ) );
% TOF_2B_hours = TOF_2B./3600

[a_res,fval] = fsolve(TOF_2H_f,a_init);


alpha_f = 2 * asinh( sqrt(s/(2*a_res)) );
beta_f = -2 * asinh( sqrt( (s-c)/(2*a_res) ) );

 TOF_res = sqrt(a_res^3 / mu) * ( ((sinh(alpha_f)) - alpha_f) - (sinh(beta_f) - beta_f) );
%  TOF_res = TOF_res/3600;

%% Type 1
elseif type_n == 1 
    
    TOF_1H_f = @(a)  sqrt((a^3) / mu) * ((sinh(2 * asinh( sqrt(s/(2*a)) )) - 2 * asinh( sqrt(s/(2*a)) ) )...
    - (sinh(2 * asinh( sqrt((s-c)/(2*a)) )) - (2 * asinh( sqrt((s-c)/(2*a)) ))  ) ) - TOF;
% TOF_2B_hours = TOF_2B./3600

[a_res,fval] = fsolve(TOF_1H_f, a_init);
    
alpha_f = 2 * asinh( sqrt(s/(2*a_res)) );
beta_f = 2 * asinh( sqrt( (s-c)/(2*a_res) ) );

 TOF_res = sqrt(a_res^3 / mu) * ( ((sinh(alpha_f)) - alpha_f) - (sinh(beta_f) - beta_f) );
%  TOF_res = TOF_res/3600;


end