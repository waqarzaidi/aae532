%
% Name: Juan F. Gutierrez
%
%
% Class: AAE 532 - Orbit Mechanics
% Assignment: Homework 11
%
% 
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Initialization

clear
close all
clc
format longG 
% format compact

%% Epoch

JD_data = 2448566.5;% For the data below
% days

JD_launch = 2441779; % date for The launch from Earth
% days


Prop_1_launch = JD_launch - JD_data;
% Propagate backwards to the launch date.

JD_Jupiter_FB = 2442385; % date for the Jupiter Fly-by
% Days

Prop_flyby_Jupiter = JD_Jupiter_FB - JD_data;
% Propagate to backwards to the Jupiter Flyby

JD_Saturn_FB = 2444118; % Date for the Saturn Fly-by
% Days

Prop_flyby_Saturn = JD_Saturn_FB - JD_data;
% Propagate to backwards to the Saturn Flyby

JD_Neptune_cross = 2447925;

Prop_Neptune_cross = JD_Neptune_cross - JD_data;

JD_Uranus = JD_Neptune_cross

%% Earth (Sun Ref) MJ2000 Ec


a_earth = 149597927.0; %km
e_earth = 0.016712542;
inc_earth = 0.001051926; %deg
RAAN_earth = 0.0; %deg
AOP_earth = 102.914377; %deg
M_earth = -58.0848626; %deg

thrs = 1e-6;
[E_earth_rad,cc_e] = Newton_iter(deg2rad(M_earth),e_earth,thrs);
% Solves for current Eccentric anomaly.
% Units: rad
E_earth = rad2deg(E_earth_rad); % deg
if E_earth < 0
    E_earth = E_earth +360;
end

theta_s_earth = 2* atand(tand(E_earth/2)*((1+e_earth)/(1-e_earth))^0.5);

if theta_s_earth < 0
    theta_s_earth = theta_s_earth +360;
end


%% Jupiter (Sun Ref) MJ2000 Ec

a_jup = 778328370.0; %km
e_jup = 0.048487326;
inc_jup = 1.30337171; %deg
RAAN_jup = 100.435428; %deg
AOP_jup = -86.2698735; %deg
M_jup = 132.596142; %deg

% thrs = 1e-4;
[E_jup_rad,cc_e] = Newton_iter(deg2rad(M_jup),e_jup,thrs);
% Solves for current Eccentric anomaly.
% Units: rad
E_jup = rad2deg(E_jup_rad); % deg
if E_jup < 0
    E_jup = E_jup +360;
end

theta_s_jup = 2* atand(tand(E_jup/2)*((1+e_jup)/(1-e_jup))^0.5);

if theta_s_jup < 0
    theta_s_jup = theta_s_jup +360;
end

%% Saturn (Sun Ref) MJ2000 Ec

a_saturn = 1426990810.0; %km
e_saturn = 0.055571251;
inc_saturn = 2.48804735; %deg
RAAN_saturn = 113.677893; %deg
AOP_saturn = -20.6752351; %deg
M_saturn = -142.586443; %deg

% thrs = 1e-4;
[E_saturn_rad,cc_e] = Newton_iter(deg2rad(M_saturn),e_saturn,thrs);
% Solves for current Eccentric anomaly.
% Units: rad
E_saturn = rad2deg(E_saturn_rad); % deg
if E_saturn < 0
    E_saturn = E_saturn +360;
end

theta_s_saturn = 2* atand(tand(E_saturn/2)*((1+e_saturn)/(1-e_saturn))^0.5);

if theta_s_saturn < 0
    theta_s_saturn = theta_s_saturn +360;
end

%% Uranus (Sun Ref) MJ2000 Ec

a_ura = 2869621890.0; %km
e_ura = 0.047296156;
inc_ura = 0.773488651; %deg
RAAN_ura = 73.9974724; %deg
AOP_ura = 96.6448354; %deg
M_ura = 107.683339; %deg

% thrs = 1e-4;
[E_ura_rad,cc_e] = Newton_iter(deg2rad(M_ura),e_ura,thrs);
% Solves for current Eccentric anomaly.
% Units: rad
E_ura = rad2deg(E_ura_rad); % deg
if E_ura < 0
    E_ura = E_ura +360;
end

theta_s_ura = 2* atand(tand(E_ura/2)*((1+e_ura)/(1-e_ura))^0.5);

if theta_s_ura < 0
    theta_s_ura = theta_s_ura +360;
end

%% Netptune (Sun Ref) MJ2000 Ec

a_nep = 4496638040.0; %km
e_nep = 0.008599220;
inc_nep = 1.77010817; %deg
RAAN_nep = 131.780238; %deg
AOP_nep = -87.1124809; %deg
M_nep = -117.599104; %deg

% thrs = 1e-4;
[E_nep_rad,cc_e] = Newton_iter(deg2rad(M_nep),e_nep,thrs);
% Solves for current Eccentric anomaly.
% Units: rad
E_nep = rad2deg(E_nep_rad); % deg
if E_nep < 0
    E_nep = E_nep +360;
end

theta_s_nep = 2* atand(tand(E_nep/2)*((1+e_nep)/(1-e_nep))^0.5);

if theta_s_nep < 0
    theta_s_nep = theta_s_nep +360;
end