%----------------------------- Begin Function -----------------------------
%Purpose:
%--------
%Uses the Newton iteration approach to calculate the Eccentric Anomaly
%
%Inputs:
%-------
% M        Mean anomaly (radians)
% e        Eccentricy
% k        number of iterations
%
%Outputs:
%--------
% E_iter           The resultant Eccentric anomaly (radians)
%
%References:
%-----------
% Orbit Mechanics Notes (G8)
%
%Programed by: 
%-------------
%Juan F. Gutierrez
%--------------------------------------------------------------------------

function [E_iter,cc] = Newton_iter(M,e,thrs)

E = M;
E_iter = 0;
% thrs = 1e-3;
cc = 0;
while  abs(E - E_iter) > thrs
    
    E_iter = E;
    E = E - ((E - e*sin(E) - M)/(1 - e*cos(E)));
    cc= cc+1;
end