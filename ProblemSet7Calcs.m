clear all; close all; clc; 

% Problem Set 7 Calculations 
% Constants
global Re mu_e j2
Re      = 6378.137;      % km
Rm      = 3396.19;       % km
Rv      = 6051.80;       % km
Rp      = 1195.00;       % km
aE      = 149649952;     % km
aP      = 5868125070;   % km 
mu_e    = 398600.4418;   % km^3/s^2 
mu_m    = 42828.37190;   % km^3/s^2 
mu_v    = 324858.592079; % km^3/s^2
mu_p    = 873.7674473980320; % km^3/s^2
mu_s    = 132712200000.00; 
j2      = 0; %0.0010826269;
deg2rad = pi/180; 
rad2deg = 180/pi;

%% Problem 2
% Given
e     = 0.7;
a     = 8*Rv;
incl  = 30;
RA    = 60;
w     = 90;
ea1   = -45*deg2rad;
ta2   = 150;
phi   = 40;
beta  = -30;

%% part (a)
ta1   = 2*atan(((1+e)/(1-e))^(0.5)*tan(ea1/2));
ea2   = 2*atan(((1+e)/(1-e))^(-0.5)*tand(ta2/2));
P     = 2*pi*sqrt(a^3/mu_v)/3600;
TF1   = (sqrt(a^3/mu_v)*((ea1-e*sin(ea1))))/3600;
TF2   = (sqrt(a^3/mu_v)*((ea2-e*sin(ea2))))/3600;
TOF   = abs(TF1) + TF2;

%% part (b)
r     = a*(1-e^2)/(1+e*cos(ta2));
v     = sqrt(mu_v/(2/r - 1/a));
FPA   = atand(e*sind(ta2)/(1+e*(cosd(ta2))));
alpha = phi - FPA;
dvVCN = [cosd(beta)*cosd(alpha);cosd(beta)*sind(alpha);sind(beta)];
dvRTH = [cosd(beta)*sind(phi);cosd(beta)*cosd(phi);sind(beta)];
OoOp  = abs(dvRTH(3))/norm(dvRTH)*100;

%% part (c) 
p     = a*(1-e^2);
h     = sqrt(mu_v*p);
coe2  = [h e RA*deg2rad incl*deg2rad w*deg2rad (ta2)*deg2rad];

% Get post maneuver inertial state using keplerian elements
[r2minus, v2minus] = sv_from_coe(coe2,mu_v);

% Get radial frame unit vectors
[xrdl, yrdl, zrdl] = eci2rdl (r2minus, v2minus);

% Build Direction Cosine Matrix (Inertial to Radial)
DCM = [xrdl;yrdl;zrdl];

gamma = w + ta2;
Rot = [cosd(RA)*cosd(gamma)-sind(RA)*cosd(incl)*sind(gamma),...
            -cosd(RA)*sind(gamma)-sind(RA)*cosd(incl)*cosd(gamma),...
            sind(RA)*sind(incl);
            sind(RA)*cosd(gamma)+cosd(RA)*cosd(incl)*sind(gamma),...
            -sind(RA)*sind(gamma)+cosd(RA)*cosd(incl)*cosd(gamma),...
            -cosd(RA)*sind(incl);
            sind(incl)*sind(gamma),...
            sind(incl)*cosd(gamma),...
            cosd(incl)];

dvXYZ = DCM\dvRTH;

v2plus = (v2minus' + dvXYZ)';

coe = coe_from_sv(r2minus,v2plus,mu_v);

% Get Flight Path angle of post maneuver orbit
p2plus = coe(7)*(1-coe(2)^2);
h2plus = norm(cross(r2minus,v2plus));
h2hatplus = cross(r2minus,v2plus)/norm(cross(r2minus,v2plus));
FPAplus = acosd(h2plus/norm(r2minus)/norm(v2plus));
dot(r2minus,v2plus);

a2plus  = -mu_v/(norm(v2plus)^2-2*mu_v/norm(r2minus));
e2plus  = sqrt(1-p2plus/a2plus);
ta2plus = acosd(1/e2plus*(p2plus/norm(r2minus)-1));
ea2plus = 2*atan(((1+e2plus)/(1-e2plus))^(-0.5)*tand(ta2plus/2));
tp2plus = (sqrt(a2plus^3/mu_v)*((ea2plus-e2plus*sin(ea2plus))))/3600;

% Get radial frame unit vectors
[xrdl, yrdl, zrdl] = eci2rdl (r2minus, v2plus);

% Build Direction Cosine Matrix (Inertial to Radial)
DCM = [xrdl;yrdl;zrdl];

Rot2 = inv(DCM);

i2plus  = acosd(Rot2(3,3));
RA2plus = atan3(Rot2(1,3),-Rot2(2,3));
aol2plus = atan3(Rot2(3,1),Rot2(3,2));
w2plus = aol2plus*180/pi - ta2plus;

rp2plus = a2plus*(1-e2plus)
ra2plus = a2plus*(1+e2plus)

ener2plus = -mu_v/2/a2plus

period2plus = 2*pi*sqrt(a2plus^3/mu_v);

%% part (f)
ea2  = 90*deg2rad;
tp2 = (sqrt(a2plus^3/mu_v)*((ea2-e2plus*sin(ea2))))/3600;
r2    = a2plus*(1-e2plus*cos(ea2));
v2    = sqrt(2*(ener2plus+mu_v/r2));

f     = 1-a2plus/norm(r2minus)*(1-cos(ea2-ea2plus));
g     = (tp2-tp2plus)*3600-sqrt(a2plus^3/mu_v)*((ea2-ea2plus)-sin(ea2-ea2plus)); 
fdot  = -sqrt(mu_v*a2plus)/r2/norm(r2minus)*sin(ea2-ea2plus);
gdot  = 1-a2plus/r2*(1-cos(ea2-ea2plus));

[f;g;fdot;gdot]

rvec  = r2minus*f + v2plus*g;
vvec  = r2minus*fdot + v2plus*gdot;

TA    = linspace(0,360,100);
for i = 1:size(TA,2)
    r(i) =  p/(1+e*cosd(TA(1,i)));
%     r1(i) = p1/(1+e1*cos(TA(1,i)*pi/180));
end

for i = 1:size(TA,2)
    e_r(i) = r(i)*cosd(TA(i));
    theta_r(i) = r(i)*sind(TA(i));
%     e_r1(i) = r1(i)*cos(TA(i)*pi/180);
%     theta_r1(i) = r1(i)*sin(TA(i)*pi/180);    
end
% figure(1)
% plot(e_r,theta_r,'r-','LineWidth',2); grid on; hold on;
% % plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
% plot(0,0,'bo',0,0,'bx');
% axis equal
% xlabel('ehat, km'); ylabel('phat, km');

%% Problem 3
% part (a) (i)
aA  = 1.04*Re;
aB  = 6.6*Re;
aT1 = (aA+aB)/2;
eT1 = 1-aA/aT1;

% Orbit 1
h1  = sqrt(2*mu_e)*sqrt(aA*aA/(aA+aA));
vA1 = h1/aA;

% Orbit 2 
h2  = sqrt(2*mu_e)*sqrt(aA*aB/(aA+aB));
vA2 = h2/aA;
vB2 = h2/aB;

% Orbit 3 
h3  = sqrt(2*mu_e)*sqrt(aB*aB/(aB+aB));
vB3 = h3/aB;

% Total Delta-V 
dvA = vA2 - vA1; 
dvB = vB3 - vB2;

tdv = dvA + dvB;

% TOF
TOF1 = pi*sqrt(aT1^3/mu_e)/3600;

% part (a) (ii)
aA  = 1.04*Re;
aB  = 6.6*Re;
eT  = (aA-aB)/(aB*cosd(180)-aA*cosd(60));
aT  = aB/(1+eT);
rT1 = aT*(1-eT^2)/(1+eT*cosd(60));
rT2 = aT*(1-eT^2)/(1+eT*cosd(180));

% Orbit 1 
vA1 = sqrt(mu_e/aA);

% Orbit 2                           
vA2 = sqrt(2*(mu_e/rT1-mu_e/2/aT)); 
% FPA = acosd(sqrt(aT*(1-eT^2)*mu_e)/rT1/vA2);
FPA = atand((eT*sind(60))/(1+eT*cosd(60)));

vB2 = sqrt(2*(mu_e/rT2-mu_e/2/aT));

% Orbit 3 
vB3 = sqrt(mu_e/aB);

% Total Delta-V 
% dvA = vA2 - vA1;     % use the fucking cosine law
dvA    = sqrt(vA1^2 + vA2^2 - 2*vA1*vA2*cosd(FPA));
beta  = asind(vA2/dvA*sind(FPA));
alpha = 180 - beta;

dvB = vB3 - vB2;

tdv = dvA + dvB;

dvA*[sind(beta);cosd(beta)]

% TOF
TOF = pi*sqrt(aT^3/mu_e)/3600;

% part (c)
nd  = sqrt(mu_e/aB^3);

phi = (pi - nd*(TOF1*3600))*180/pi;

%% All plots for Problem 3
TA    = linspace(0,360,100);
for i = 1:size(TA,2)
    r(i) =  aA;
    r1(i) = aB;
    r2(i) = aT1*(1-eT1^2)/(1+eT1*cos(TA(1,i)*pi/180));
    r3(i) = aT*(1-eT^2)/(1+eT*cos(TA(1,i)*pi/180));
end

for i = 1:size(TA,2)
    e_r(i) = r(i)*cosd(TA(i));
    theta_r(i) = r(i)*sind(TA(i));
    e_r1(i) = r1(i)*cos(TA(i)*pi/180);
    theta_r1(i) = r1(i)*sin(TA(i)*pi/180);    
    e_r2(i) = r2(i)*cos(TA(i)*pi/180);
    theta_r2(i) = r2(i)*sin(TA(i)*pi/180);
    e_r3(i) = r3(i)*cos(TA(i)*pi/180);
    theta_r3(i) = r3(i)*sin(TA(i)*pi/180);    
end
figure(1)
plot(e_r,theta_r,'r-','LineWidth',2); grid on; hold on;
plot(e_r1,theta_r1,'b-','LineWidth',2); hold on;
plot(e_r2,theta_r2,'g-','LineWidth',2); hold on;
plot(e_r3,theta_r3,'m-','LineWidth',2); hold on;
plot(0,0,'bo',0,0,'bx');
axis equal
xlabel('ehat, km'); ylabel('phat, km');
legend('LEO','GEO','Hohmann','Non-Elliptical')


%% Problem 4
% part (a)
aA  = aE;
aB  = aP;
aT  = (aA+aB)/2;

% Orbit 1
h1  = sqrt(2*mu_s)*sqrt(aA*aA/(aA+aA));
vA1 = h1/aA;

% Orbit 2 
h2  = sqrt(2*mu_s)*sqrt(aA*aB/(aA+aB));
vA2 = h2/aA;
vB2 = h2/aB;

% Orbit 3 
h3  = sqrt(2*mu_s)*sqrt(aB*aB/(aB+aB));
vB3 = h3/aB;

% Total Delta-V 
dvA = vA2 - vA1; 
dvB = vB3 - vB2;

tdv = dvA + dvB;

TOF1 = pi*sqrt(aT^3/mu_s)/86400/365.25;

% part (c) 
eP  = 0.24758;
rP  = aB*(1-eP);
aT  = (aA+rP)/2;

% Orbit 1
h1  = sqrt(2*mu_s)*sqrt(aA*aA/(aA+aA));
vA1 = h1/aA

% Orbit 2 
h2  = sqrt(2*mu_s)*sqrt(aA*rP/(aA+rP));
vA2 = h2/aA
vB2 = h2/rP

% Orbit 3 
vB3 = sqrt(2*(mu_s/rP-mu_s/2/aP))

% Total Delta-V 
dvA = vA2 - vA1; 
dvB = vB3 - vB2;

tdv = dvA + dvB;

TOF = pi*sqrt(aT^3/mu_s)/86400/365.25;

% part (c)
jd1 = jday(2006, 1, 19, 0, 0, 0);
jd2 = jday(2015, 7, 14, 0, 0, 0);
TOF = jd2-jd1; 

np  = sqrt(mu_s/aP^3)

phi = (pi  - TOF1*86400*365.25*np)*180/pi
