clear all; close all; clc; 

% Problem Set 4 Calculations 
% Constants
Re = 6378.137;     % km
mu = 398600.4418;  % km^3/s^2 

% Given
a  = 6*Re; 
e  = 0.4; 
Eo = -pi/2; 

% Part (a) 
p  = a*(1-e^2);
rp = a*(1-e);
ra = a*(1+e);
P  = 2*pi*sqrt(a^3/mu);
E  = -mu/2/a;
r0 = a*(1-e*cos(Eo));
v0 = sqrt(2*(E+mu/r0));
f0 = 2*atan(((1+e)/(1-e))^0.5*tan(Eo/2));
h  = sqrt(mu*p);
g0 = atan(e*sin(f0)/(1+e*cos(f0)));

disp('True Anomaly, deg')
disp(mod(f0*180/pi,360))

disp('Flight Path Angle, deg')
disp(mod(g0*180/pi,360))

% Part (b) 
rh   = [cos(f0);sin(f0)]*r0;
v1   = [sin(g0);cos(g0)]*v0;
v2   = v1(1)*[cos(f0);sin(f0)];
v3   = v1(2)*[-sin(f0);cos(f0)];
vh   = v2+v3;

% atand(e*sin(f0)/(1+e*cos(f0)))
% acosd(h/r0/v0)
% acosd(dot(rh,vh)/(norm(rh)*norm(vh)))


ff   = pi/2; 
rf   = p/(1+e*cos(ff));
vf   = sqrt(2*(E+mu/rf));
Ef   = 2*atan(((1-e)/(1+e))^(0.5)*tan(ff/2));
gf   = atan(e*sin(ff)/(1+e*cos(ff)));
rhf  = [cos(ff);sin(ff)]*rf;
v1f  = [sin(gf);cos(gf)]*vf;
v2f  = v1f(1)*[cos(ff);sin(ff)];
v3f  = v1f(2)*[-sin(ff);cos(ff)];
vhf  = v2f+v3f;

% Part (c) 
tp   = (Ef-e*sin(Ef))*P/2/pi;
to   = (Eo-e*sin(Eo))*P/2/pi;

TOF  = (sqrt(a^3/mu)*((Ef-e*sin(Ef))-(Eo-e*sin(Eo))))/3600;

% Part (d) 
% Define a vector with true anomalies from 0 to 360

TA = linspace(0,360,100);

% Compute a vector with positions

n = size(TA,2);

for i = 1:n
    r(i) = p/(1+e*cos(TA(1,i)*pi/180));
end

for i = 1:n
    v(i) = sqrt(2*(E+mu/r(i)));
end

for i = 1:n
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
end

for i = 1:n
    e_v(i) = -v(i)*sin(TA(i)*pi/180);
    theta_v(i) = v(i)*cos(TA(i)*pi/180);
end

% figure(1)
% plot(e_r,theta_r,'r-');
% hold on
% plot(0,0,'o');

%% Problem 3
% Part (a) 
vinf = 2.65;               % km/s
h    = 500;                % km
mu   = 42828.3719012840;
Rm   = 3396.19;            % Martian Radius, km
rp   = h + Rm;             % km
a    = mu/vinf^2;
e    = rp/a+1;
h    = sqrt(a*mu*(e^2-1)); % km^2/s^2 
E    = mu/2/a;
del  = 2*asind(1/e);
f    = acosd(-1/e); 
p    = a*(e^2-1);

% ehat and phat components at a specified TA
TA   = -110*pi/180;
rRp  = a*(e^2-1)/(1+e*cos(TA));
vRp  = sqrt(2*(mu/2/a+mu/rRp));
g    = atan(e*sin(TA)/(1+e*cos(TA)));
rh   = [cos(TA);sin(TA)]*rRp
v1   = [sin(g);cos(g)]*vRp;
v2   = v1(1)*[cos(TA);sin(TA)];
v3   = v1(2)*[-sin(TA);cos(TA)];
vh   = v2+v3

% Part (b) 
b    = a*(sqrt(e^2-1));
fs   = acos(b/(rp+a));
rOut = a*(e^2-1)/(1+e*cos(fs));
x    = -a-rp+r*cos(fs);
y    = r*sin(fs);
vOut = sqrt(2*(mu/2/a+mu/rOut));
g    = atan(e*sin(fs)/(1+e*cos(fs)));

% Part (c) 
rh   = [cos(fs);sin(fs)]*rOut;
v1   = [sin(g);cos(g)]*vOut;
v2   = v1(1)*[cos(fs);sin(fs)];
v3   = v1(2)*[-sin(fs);cos(fs)];
vh   = v2+v3;

TA = linspace(0,360,100);

% Compute a vector with positions

n = size(TA,2);

for i = 1:n
    r(i) = a*(e^2-1)/(1+e*cos(TA(1,i)*pi/180));
end

for i = 1:n
    v(i) = sqrt(2*(mu/2/a+mu/r(i)));
end

for i = 1:n
    e_r(i) = r(i)*cos(TA(i)*pi/180);
    theta_r(i) = r(i)*sin(TA(i)*pi/180);
end

for i = 1:n
    e_v(i) = -v(i)*sin(TA(i)*pi/180);
    theta_v(i) = v(i)*cos(TA(i)*pi/180);
end

figure(1)
plot(e_r,theta_r,'r-');
hold on
plot(0,0,'o');
xlim([-10000 10000]);